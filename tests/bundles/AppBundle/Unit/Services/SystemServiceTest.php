<?php

namespace Tests\Bundles\AppBundle\Services;

use AppBundle\Service\SystemService;
use PHPUnit\Framework\TestCase;

class SystemServiceTest extends TestCase
{
    public function getSystemService()
    {
        return new SystemService();
    }
    
    public function testGetSetterFunctionName_String_ReturnString()
    {
        $systemService = $this->getSystemService();
        $propertyName = 'property';
        
        $result = $systemService->getSetterFunctionName($propertyName);
        
        $expected = 'setProperty';
        $this->assertEquals($expected, $result);
    }

    public function testGetGetterFunctionName_String_ReturnString()
    {
        $systemService = $this->getSystemService();
        $propertyName = 'property';

        $result = $systemService->getGetterFunctionName($propertyName);

        $expected = 'getProperty';
        $this->assertEquals($expected, $result);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetSetterFunctionName_EmptyString_ThrowException()
    {
        $systemService = $this->getSystemService();
        $propertyName = '';
        
        $systemService->getSetterFunctionName($propertyName);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetGetterFunctionName_EmptyString_ThrowException()
    {
        $systemService = $this->getSystemService();
        $propertyName = '';

        $systemService->getGetterFunctionName($propertyName);
    }
}