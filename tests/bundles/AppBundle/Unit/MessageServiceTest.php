<?php

namespace Tests\Bundles\AppBundle\Unit;

use AppBundle\Service\MessageService;
use PHPUnit\Framework\TestCase;
use Zend\Code\Exception\RuntimeException;

class MessageServiceTest extends TestCase
{
    /**
     * @var $messageService MessageService
     */
    private $messageService;

    private $messageConfiguration = [
        'error' => [
            100 => 'First error message',
            101 => 'Second error message',
            102 => 'Third error message'
        ],
        'success' => [
            100 => 'First success message',
            101 => 'Second success message',
            102 => 'Third success message'
        ]
    ];

    public function getMessageService()
    {
        if (!isset($this->messageService)) {
            $this->messageService = new MessageService($this->messageConfiguration);
        }

        return $this->messageService;
    }

    /**
     * @dataProvider fetchMessageCases
     *
     * @param $code
     * @param $type
     * @param $expectedMessage
     */
    public function testReturnMessage($code, $type, $expectedMessage)
    {
        $this->assertEquals($expectedMessage, $this->getMessageService()->getMessage($code, $type));
    }

    public function fetchMessageCases()
    {
        return [
            [100, MessageService::ERROR, 'First error message'],
            [102, MessageService::SUCCESS, 'Third success message']
        ];
    }

    /**
     * @expectedException RuntimeException
     */
    public function testNotExistingMessageType()
    {
        $this->getMessageService()->getMessage(100, 'not_existing_key');
    }

    /**
     * @expectedException RuntimeException
     */
    public function testNotExistingMessageKey()
    {
        $this->getMessageService()->getMessage(100, 'not_existing_key');
    }
}