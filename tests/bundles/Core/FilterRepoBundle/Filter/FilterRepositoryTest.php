<?php

namespace Tests\Bundles\Core\FilterRepoBundle\Filter;

use Core\FilterRepoBundle\Filter\DQL_Filter;
use Core\FilterRepoBundle\Filter\FilterRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit\Framework\TestCase;

class FilterRepositoryTest extends TestCase
{
    const DEFAULT_ALIAS = 'c';

    public static function getDefaultDql()
    {
        return 'SELECT ' . static::DEFAULT_ALIAS . ' FROM CategoryBundle:Category ' . static::DEFAULT_ALIAS;
    }

    public function getFilterRepositoryMock(): FilterRepository
    {
        $emMock = $this->createMock(EntityManager::class);
        $classMetadataMock = $this->createMock(ClassMetadata::class);

        return new class ($emMock, $classMetadataMock) extends FilterRepository {

            private $currentCondition;

            public function getAlias(): string
            {
                return FilterRepositoryTest::DEFAULT_ALIAS;
            }

            public function getDqlWithAlias()
            {
                return FilterRepositoryTest::getDefaultDql();
            }

            public function setCurrentDqlFilterCondition($condition)
            {
                $this->currentCondition = $condition;
            }

            public function getDqlFilterCondition(DQL_Filter $filter)
            {
                return $this->currentCondition;
            }
        };
    }

    public function testAddDqlFilter_DqlFilter_ReturnFilterArray()
    {
        $dqlFilter = $this->createMock(DQL_Filter::class);

        $repository = $this->getFilterRepositoryMock();
        $repository->addDqlFilter(get_class($dqlFilter));

        $expected = 1;
        $this->assertCount($expected, $repository->getFilters());
    }

    public function testGetDqlSummary_AddOneFilter_ReturnString()
    {
        $dqlFilter = $this->createMock(DQL_Filter::class);
        
        $repository = $this->getFilterRepositoryMock();
        $repository->setCurrentDqlFilterCondition(self::DEFAULT_ALIAS . '.parent = 3');
        $repository->addDqlFilter(get_class($dqlFilter));

        $expected = self::getDefaultDql() . ' WHERE (c.parent = 3)';

        $this->assertEquals($expected, $repository->getDqlSummary());
    }

    public function testGetDqlSummary_AddTwoFilter_ReturnString()
    {
        $dqlFilter = $this->createMock(DQL_Filter::class);

        $repository = $this->getFilterRepositoryMock();
        $repository->setCurrentDqlFilterCondition(self::DEFAULT_ALIAS . '.active = 1');
        $repository->addDqlFilter(get_class($dqlFilter));
        $repository->addDqlFilter(get_class($dqlFilter));

        $expected = self::getDefaultDql() . ' WHERE (c.active = 1) AND (c.active = 1)';

        $this->assertEquals($expected, $repository->getDqlSummary());
    }


}