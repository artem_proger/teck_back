<?php

namespace Tests\Bundles\Core\MultimediaBundle;

use Core\MultimediaBundle\Configuration;
use PHPUnit\Framework\TestCase;

class ConfigurationTest extends TestCase
{

    private function getConfiguration($config)
    {
        return new Configuration($config);
    }

    public function testGetFilesConfig_ReturnArray()
    {
        $configuration = $this->getConfiguration([
            'files' => [
                '300x300' => [
                    'extension' => '.jpg',
                    'intervention' => [
                        'fit' => [300, 300]
                    ]
                ],
                '50x50' => [
                    'extension' => '.jpg',
                    'intervention' => [
                        'fit' => [50, 50]
                    ]
                ]
            ]
        ]);

        $filesConfig = $configuration->getFilesConfig();

        $this->assertTrue(is_array($filesConfig));
    }

    public function testGetUploadDir_ReturnString()
    {
        $configuration = $this->getConfiguration([
            'upload_dir' => '/images/category'
        ]);

        $uploadDir = $configuration->getUploadDir();

        $expected = '/images/category';
        $this->assertEquals($expected, $uploadDir);
    }

    public function testGetProperty_ReturnString()
    {
        $configuration = $this->getConfiguration([
            'property' => 'image'
        ]);

        $property = $configuration->getProperty();

        $expected = 'image';
        $this->assertEquals($expected, $property);
    }
}