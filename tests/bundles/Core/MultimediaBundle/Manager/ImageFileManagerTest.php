<?php


namespace Tests\Bundles\Core\MultimediaBundle\Manager;


use AppBundle\Service\SystemService;
use CatalogBundle\Entity\Product;
use Core\MultimediaBundle\ImagePackage;
use Core\MultimediaBundle\Manager\ConfigurationManagerInterface;
use Core\MultimediaBundle\Manager\ImageFileManager;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageFileManagerTest extends TestCase
{
    const RESOURCES_DIR = '/resources';

    public function testGenerateFileName_WithoutParameters_ReturnString()
    {
        $uniqueString = 'filename';
        $systemServiceMock = $this->getSystemServiceMock($uniqueString);
        $imageFileManager = $this->getImageFileManager(null, null, $systemServiceMock);

        $filename = $imageFileManager->generateFileName();

        $this->assertEquals('filename' . ImageFileManager::DEFAULT_EXTENSION, $filename);
    }
    
    public function testGenerateFileName_WithExtensionParameter_ReturnString()
    {
        $uniqueString = 'filename';
        $systemServiceMock = $this->getSystemServiceMock($uniqueString);
        $imageFileManager = $this->getImageFileManager(null, null, $systemServiceMock);

        $extension = '.png';
        $filename = $imageFileManager->generateFileName($extension);

        $expected = 'filename.png';
        $this->assertEquals($expected, $filename);
    }

    public function testGenerateFileName_WithPostfixParameter_ReturnString()
    {
        $uniqueString = 'filename';
        $systemServiceMock = $this->getSystemServiceMock($uniqueString);
        $imageFileManager = $this->getImageFileManager(null, null, $systemServiceMock);

        $postfix = 'postfix';
        $filename = $imageFileManager->generateFileName(null, $postfix);

        $expected = 'filename_postfix' . ImageFileManager::DEFAULT_EXTENSION;
        $this->assertEquals($expected, $filename);
    }

    public function testGenerateFileName_WithExtensionPostfixParameter_ReturnString()
    {
        $uniqueString = 'filename';
        $systemServiceMock = $this->getSystemServiceMock($uniqueString);
        $imageFileManager = $this->getImageFileManager(null, null, $systemServiceMock);

        $postfix = 'postfix';
        $extension = '.png';
        $filename = $imageFileManager->generateFileName($extension, $postfix);

        $expected = 'filename_postfix.png';
        $this->assertEquals($expected, $filename);
    }
    
    public function testUploadSingle_ReturnImagePackage()
    {
        // Prepare Image mock

        /**
         * @var $fakeImage Image
         */
        $fakeImage = $this->getMockBuilder(Image::class)
            ->setMethods(['getWidth', 'getHeight', 'save'])
            ->getMock();
        $fakeImage
            ->method('getWidth')
            ->willReturn(400);
        $fakeImage
            ->method('getHeight')
            ->willReturn(300);
        $fakeImage
            ->method('save')
            ->willReturn(true);

        // Prepare ImageManager mock

        /**
         * @var $fakeImageManager ImageManager
         */
        $fakeImageManager = $this->getMockBuilder(ImageManager::class)
            ->setMethods(['make'])
            ->getMock();
        $fakeImageManager->method('make')
            ->with($this->anything())
            ->willReturn($fakeImage);


        // Create ImageFileManager

        $imageFileManager = new ImageFileManager(
            $this->getConfigurationManagerMock(),
            $fakeImageManager,
            $this->getSystemServiceMock('uniq'),
            self::RESOURCES_DIR
        );

        /**
         * @var $fakeUploadedFile UploadedFile
         */
        $fakeUploadedFile = $this->getMockBuilder(UploadedFile::class)
            ->disableOriginalConstructor()
            ->setMethods(['getPathname'])
            ->getMock();
        $fakeUploadedFile
            ->method('getPathname')
            ->willReturn('pathname');

        $filesConfig = [
            'mini' => null,
            'max' => null
        ];
        $uploadDir = '/uploads';

        $imagePackage = $imageFileManager->uploadSingle($fakeUploadedFile, $filesConfig, $uploadDir);

        $imagePackageData = $imagePackage->toArray();
        $expected = [
            'filenames' => [
                "mini" => "uniq_400x300.jpg",
                "max" => "uniq_400x300.jpg"
            ]
        ];

        $this->assertEquals(ImagePackage::class, get_class($imagePackage));
        $this->assertArraySubset($expected, $imagePackageData);
    }
    
    public function testFetchImagePackageFromEntity_EntityAndProperty_ReturnImagePackage()
    {
        $propertyName = 'testProperty';
        
        $entityMock = $this->getMockBuilder(Product::class)
            ->setMethods(['getTestProperty'])
            ->getMock();
        
        $entityMock->method('getTestProperty')
            ->willReturn([
                'filenames' => [
                    '300x400' => [],
                    '400x300' => []
                ]
            ]);
        
        $fileManager = $this->getImageFileManager();
        $result = $fileManager->fetchImagePackageFromEntity($entityMock, $propertyName);
        $expected = ImagePackage::class;
        $this->assertInstanceOf($expected, $result);
        
    }

    /**
     * @expectedException \Exception
     *
     * @throws \Exception
     */
    public function testFetchImagePackageFromEntity_EntityAndWrongProperty_ThrowException()
    {
        $propertyName = 'testWrongProperty';

        $entityMock = $this->getMockBuilder(Product::class)
            ->setMethods(['getTestProperty'])
            ->getMock();

        $entityMock->method('getTestProperty')
            ->willReturn([
                'filenames' => [
                    '300x400' => [],
                    '400x300' => []
                ]
            ]);

        $fileManager = $this->getImageFileManager();
        $result = $fileManager->fetchImagePackageFromEntity($entityMock, $propertyName);
        $expected = ImagePackage::class;
        $this->assertInstanceOf($expected, $result);

    }
    
    public function testFillEntityProperty_PropertyName_ReturnProperty()
    {
        $systemServiceMock = $this->getMockBuilder(SystemService::class)
            ->setMethods(['getSetterFunctionName'])
            ->getMock();
        $systemServiceMock->method('getSetterFunctionName')
            ->with($this->anything())
            ->willReturn('setProperty');
        $imageManager = $this->getImageFileManager(
            null, null, $systemServiceMock
        );
        $entityMock = $this->getEntityMock();

        $propertyName = 'property';
        $data = 'value';

        $imageManager->fillEntityProperty($entityMock, $propertyName, $data);
        $result = $entityMock->getProperty();

        $expected = 'value';
        $this->assertEquals($expected, $result);
    }


    /**
     * @expectedException \Exception
     */
    public function testFillEntityProperty_WrongPropertyName_ThrowsException()
    {
        $systemServiceMock = $this->getMockBuilder(SystemService::class)
            ->setMethods(['getSetterFunctionName'])
            ->getMock();
        $systemServiceMock->method('getSetterFunctionName')
            ->with($this->anything())
            ->willReturn('setWrongProperty');
        $imageManager = $this->getImageFileManager(
            null, null, $systemServiceMock
        );
        $entityMock = $this->getEntityMock();

        $propertyName = 'wrongProperty';
        $data = 'value';

        $imageManager->fillEntityProperty($entityMock, $propertyName, $data);
        $result = $entityMock->getProperty();

        $expected = 'value';
        $this->assertEquals($expected, $result);
    }



    private function getConfigurationManagerMock()
    {
        return $this->getMockBuilder(ConfigurationManagerInterface::class)
            ->getMock();
    }

    private function getSystemServiceMock(string $systemUniqueString = null)
    {
        $mock = $this->getMockBuilder(SystemService::class)
            ->setMethods(['getUniqueString'])
            ->getMock();


        if ($systemUniqueString) {
            $mock->method('getUniqueString')
                ->willReturn($systemUniqueString);
        }

        return $mock;
    }

    private function getImageManagerMock(): PHPUnit_Framework_MockObject_MockObject
    {
        return $this->getMockBuilder(ImageManager::class)
            ->getMock();
    }

    private function getImageFileManager(
        ConfigurationManagerInterface $configurationManager = null,
        ImageManager $imageManager = null,
        SystemService $systemService = null
    ): ImageFileManager {

        /**
         * @var $fakeConfigurationManager ConfigurationManagerInterface
         */
        $fakeConfigurationManager = $configurationManager
            ? $configurationManager
            : $this->getConfigurationManagerMock();

        $fakeImageManager = $imageManager
            ? $imageManager
            : $this->getImageManagerMock();

        $fakeSystemService = $systemService
            ? $systemService
            : $this->getSystemServiceMock();

        $imageFileManager = new ImageFileManager(
            $fakeConfigurationManager,
            $fakeImageManager,
            $fakeSystemService,
            self::RESOURCES_DIR
        );

        return $imageFileManager;
    }

    private function getEntityMock()
    {
        return new class {

            private $property;

            /**
             * @return mixed
             */
            public function getProperty()
            {
                return $this->property;
            }

            /**
             * @param mixed $property
             */
            public function setProperty($property)
            {
                $this->property = $property;
            }
        };
    }


}