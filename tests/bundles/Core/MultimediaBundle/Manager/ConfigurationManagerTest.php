<?php

namespace Tests\Bundles\Core\MultimediaBundle\Manager;

use Core\MultimediaBundle\Configuration;
use Core\MultimediaBundle\Loader\LoaderInterface;
use Core\MultimediaBundle\Manager\ConfigurationManager;
use PHPUnit\Framework\TestCase;

class ConfigurationManagerTest extends TestCase
{
    const FAKE_CONFIGURATRIONS = [
        'CatalogBundle\Entity\Category' => [
            'category_thumbnail' => [
                'upload_dir' => '/images/category',
                'files' => [
                    '300x300' => [
                        'extension' => '.jpg',
                        'intervention' => [
                            'fit' => [300, 300]
                        ]
                    ],
                    '50x50' => [
                        'extension' => '.jpg',
                        'intervention' => [
                            'fit' => [50, 50]
                        ]
                    ]
                ]
            ]
        ]
    ];

    public function getConfigurationManager(): ConfigurationManager
    {
        /**
         * @var $uploadConfigLoaderMock LoaderInterface
         */
        $uploadConfigLoaderMock = $this->getMockBuilder(LoaderInterface::class)
            ->setMethods(['getMappings'])
            ->getMock();
        $uploadConfigLoaderMock->expects($this->once())
            ->method('getMappings')
            ->willReturn(self::FAKE_CONFIGURATRIONS);

        return new ConfigurationManager($uploadConfigLoaderMock);
    }

    public function testGetNamedConfiguration_ValidClassValidName_ReturnConfiguration()
    {
        $configurationManager = $this->getConfigurationManager();

        $namedConfiguration = $configurationManager->getConfig('CatalogBundle\Entity\Category', 'category_thumbnail');

        $this->assertEquals(Configuration::class, get_class($namedConfiguration));
    }

    /**
     * @expectedException \Exception
     */
    public function testGetNamedConfiguration_InvalidClass_ThrowException()
    {
        $configurationManager = $this->getConfigurationManager();

        $configurationManager->getConfig('asdasdzxcznotexists', 'category_thumbnail');
    }

    /**
     * @expectedException \Exception
     */
    public function testGetNamedConfiguration_InvalidName_ThrowException()
    {
        $configurationManager = $this->getConfigurationManager();

        $configurationManager->getConfig('CatalogBundle\Entity\Category', 'asdasdzxczcsaddawsd');
    }
}