<?php

namespace Tests\Bundles\CatalogBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Tests\Helpers\Traits\HttpHelper;
use Tests\Bundles\UserBundle\DataFixtures\UserFixture;
use UserBundle\Entity\User;

class CategoryControllerTest extends WebTestCase
{
    use HttpHelper;
    
    public function setUp()
    {
        $this->initClient(true);
        $this->fixtures = $this->loadFixtures([
            UserFixture::class
        ])->getReferenceRepository();

        /**
         * @var $user User
         */
        $user = $this->fixtures->getReference('auth_user');
        $this->loginAs($user, 'api');
    }

    public function runCategoryTreeFixtures()
    {
        $this->fixtureReferences = $this->loadFixtureFiles([
            __DIR__ . '/../../../fixtures/category/category-tree.fixture.yml'
        ], true);
    }

    public function runCategoryBasicFixtures()
    {
        $this->fixtureReferences = $this->loadFixtureFiles([
            __DIR__ . '/../../../fixtures/category.fixture.yml'
        ], true);
    }

    public function testCreate()
    {
        $this->createRequest([
            'title' => 'test_category'
        ]);


        $response = $this->client->getResponse();
        $responseData = $this->extractResponseObject($response);

        $this->assertStatusCode(201, $this->client);
        $this->assertArrayHasKey('id', $responseData);
        $this->assertArrayHasKey('title', $responseData);
        $this->assertArrayHasKey('children', $responseData);


        // Testing Not Blank

        $this->createRequest([
            'title' => ''
        ]);

        $responseData = $this->extractResponseObject($this->client->getResponse());

        $this->assertStatusCode(400, $this->client);
        $this->assertArrayHasKey('errors', $responseData);
        $this->assertArrayHasKey('title', $responseData['errors']);

        // Testing unique

        $this->createRequest(['title' => 'test_category']);
        $this->assertStatusCode(400, $this->client);

        $this->assertArrayHasKey('title', $this->extractResponseObject(
            $this->client->getResponse()
        )['errors']);
    }

    public function testUpdate()
    {
        $this->runCategoryBasicFixtures();
        $this->updateRequest($this->fixtureReferences['update_category']->getId(),
            ['title' => 'new title']
        );
        $this->assertStatusCode(200, $this->client);
    }

    public function testCollection()
    {
        $this->collectionRequest();
        $this->assertStatusCode(200, $this->client);
    }

    public function testDetails()
    {
        $this->runCategoryBasicFixtures();
        $this->detailsRequest($this->fixtureReferences['details_category']->getId());
        $this->assertStatusCode(200, $this->client);
        $this->detailsRequest(0);
        $this->assertStatusCode(404, $this->client);
    }

    public function testAssign()
    {
        $this->runCategoryBasicFixtures();
        $this->assignRequest(
            $this->fixtureReferences['update_category']->getId(),
            $this->fixtureReferences['details_category']->getId()
        );

        $this->assertStatusCode(200, $this->client);
        $this->detailsRequest($this->fixtureReferences['details_category']->getId());
        $responseData = $this->extractResponseObject($this->client->getResponse());
        $this->assertCount(1, $responseData['children']);
    }

    public function testRemove()
    {
        $this->runCategoryBasicFixtures();
        $this->collectionRequest();
        $responseData = $this->extractResponseObject($this->client->getResponse());
        $this->assertCount(7, $responseData['data']);
        $removingCategory = $responseData['data'][0];
        $this->removeRequest($removingCategory['id']);
        $this->assertStatusCode(200, $this->client);
        $this->collectionRequest();
        $responseData = $this->extractResponseObject($this->client->getResponse());
        $this->assertCount(6, $responseData['data']);
    }

    public function testThumbnail()
    {
        $this->runCategoryBasicFixtures();
        copy(__DIR__ . '/../Resources/valid.jpg', __DIR__ . '/../Resources/temp.jpg');

        $validThumbnail = new UploadedFile(__DIR__ . '/../Resources/temp.jpg', 'temp.jpg', 'image/jpg');
        $this->thumbnailRequest(
            $this->fixtureReferences['update_category']->getId(),
            $validThumbnail
        );

        $this->assertStatusCode(200, $this->client);
    }

    public function testCollectionLeaves()
    {
        $this->collectionTreeRequest();
        $responseData = $this->extractResponseObject($this->client->getResponse());
        $this->assertCount(5, $responseData['data']);
    }

    private function createRequest($data)
    {
        $this->client->request(
            'POST',
            $this->api_prefix . 'category',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data),
            false
        );
    }

    private function collectionRequest()
    {
        $this->client->request(
            'GET',
            $this->api_prefix .'category',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            null,
            false
        );
    }

    private function collectionTreeRequest()
    {
        $this->client->request(
            'GET',
            $this->api_prefix .'category',
            [
                'has-children' => 0
            ],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            null,
            false
        );
    }

    private function detailsRequest($id)
    {
        $this->client->request(
            'GET',
            $this->api_prefix .'category/'.$id,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            null,
            false
        );
    }

    private function updateRequest($id, $data)
    {
        $this->client->request(
            'PUT',
            $this->api_prefix . "category/$id",
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data),
            false
        );
    }

    private function removeRequest($id)
    {
        $this->client->request(
            'DELETE',
            $this->api_prefix . "category/$id",
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            null,
            false
        );
    }

    private function assignRequest($id, $parent_id)
    {
        $this->client->request(
            'PUT',
            $this->api_prefix . "category/$id/assign",
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'parent_id' => $parent_id
            ]),
            false
        );
    }

    private function thumbnailRequest($id, UploadedFile $file)
    {
        $this->client->request(
            'POST',
            $this->api_prefix . "category/$id/thumbnail",
            [],
            [
                'thumbnail' => $file
            ]
        );
    }
}