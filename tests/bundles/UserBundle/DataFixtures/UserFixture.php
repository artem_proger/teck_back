<?php

namespace Tests\Bundles\UserBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManagerInterface;

class UserFixture extends Fixture
{
    /**
     * Load users
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /**
         * @var $userManager UserManagerInterface
         */
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setEnabled(true);
        $user->setUsername('admin');
        $user->setPlainPassword('admin');
        $user->setEmail('qwe@qwe.qwe');

        $userManager->updateUser($user);
        $this->setReference('auth_user', $user);
    }
}