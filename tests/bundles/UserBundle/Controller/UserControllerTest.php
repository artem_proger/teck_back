<?php

namespace Tests\Bundles\UserBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\Helpers\Traits\HttpHelper;
use Tests\Bundles\UserBundle\DataFixtures\UserFixture;

class UserControllerTest extends WebTestCase
{
    use HttpHelper;

    /**
     * Setting up test
     */
    public function setUp()
    {
        $this->initClient();
        $this->fixtures = $this->loadFixtures([
            UserFixture::class
        ])->getReferenceRepository();
    }

    /**
     * Test user login
     *
     * @dataProvider loginCases
     * @param array $data
     * @param int $status
     */
    public function testLogin(array $data, int $status)
    {
        $this->loginRequest($data);

        $this->assertStatusCode($status, $this->client);
    }

    /**
     * Test refresh token
     */
    public function testRefresh()
    {
        $data = [
            'username' => 'admin',
            'password' => 'admin'
        ];

        $this->loginRequest($data);

        $response = $this->client->getResponse();
        $refreshToken = $this->extractResponseObject($response)['refresh_token'];

        $data = [
            'refresh_token' => $refreshToken
        ];

        $this->refreshRequest($data);

        $this->assertStatusCode(Response::HTTP_OK, $this->client);

    }

    /**
     * Login cases
     *
     * @return array
     */
    public function loginCases()
    {
        return [
            [[
                'username' => 'admin',
                'password' => 'admin'
            ], Response::HTTP_OK],
            [[
                'username' => 'bad',
                'password' => 'credentials'
            ], Response::HTTP_UNAUTHORIZED]
        ];
    }

    /**
     * @param $data
     */
    private function loginRequest($data)
    {
        $this->client->request(
            'POST',
            'api/login',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data),
            false
        );
    }

    /**
     * @param $data
     */
    private function refreshRequest($data)
    {
        $this->client->request(
            'POST',
            'api/token/refresh',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data),
            false
        );
    }
}