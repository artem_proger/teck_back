<?php

namespace Tests\Bundles\UserBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\Helpers\Traits\HttpHelper;

class RegistrationControllerTest extends WebTestCase
{
    use HttpHelper;
    
    protected function setUp()
    {
        parent::setUp();
        $this->initClient();
        $this->fixtures = $this->loadFixtures([]);
    }

    /**
     * @dataProvider registrationCases
     * @param array $data
     * @param int $status
     */
    public function testRegistration(array $data, int $status)
    {
        $this->registrationRequest($data);
        $this->assertStatusCode($status, $this->client);
    }

    /**
     * Registration cases
     * 
     * @return array
     */
    public function registrationCases()
    {
        return [
            [[
                'username' => 'turboazot',
                'email' => 'qwe@qwe.qwe',
                'password' => '636261'
            ], Response::HTTP_CREATED],
            [[
                'username' => 'qwe',
                'email' => 'qwe@qwe.qwe',
                'password' => 1
            ], Response::HTTP_BAD_REQUEST]
        ];
    }

    /**
     * Registration request
     *
     * @param $data
     */
    public function registrationRequest($data)
    {
        $this->client->request(
            'POST',
            $this->api_prefix . 'registration',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );
    }
    
    
}
