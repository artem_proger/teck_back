<?php

namespace Tests\Helpers\Traits;

use Doctrine\Common\DataFixtures\ReferenceRepository;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\Response;

trait HttpHelper
{
    private $api_prefix = 'api/';

    /**
     * @var $client Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $defaultClientOptions = [
        'HTTP_ACCEPT' => 'application/json'
    ];

    /**
     * @var $fixtures ReferenceRepository
     */
    protected $fixtures;

    protected $fixtureReferences = [];

    public function initClient($auth = false)
    {
        $this->client = $this->makeClient($auth, $this->defaultClientOptions);
    }
    
    private function extractResponseObject(Response $response)
    {
        return json_decode($response->getContent(), true);
    }
}