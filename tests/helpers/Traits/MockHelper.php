<?php

namespace Tests\Helpers\Traits;

trait MockHelper
{
    public function getClosedMethod($className, $methodName): \ReflectionMethod
    {
        $class = new \ReflectionClass($className);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        return $method;
    }
}