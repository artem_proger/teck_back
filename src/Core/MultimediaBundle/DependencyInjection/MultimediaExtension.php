<?php

namespace Core\MultimediaBundle\DependencyInjection;

use Core\MultimediaBundle\Loader\ImageLoader;
use Core\MultimediaBundle\Loader\UploadConfigLoader;
use Core\MultimediaBundle\Manager\ImageFileManager;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Yaml\Yaml;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class MultimediaExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setDefinition(
            'multimedia.image_file_manager',
            new Definition(ImageFileManager::class, [
                new Reference('multimedia.config_manager'),
                new Reference('multimedia.intervention_image'),
                new Reference('app.system'),
                $config['resources_dir']
            ])
        );
    }

    public function getNamespace()
    {
        return 'multimedia_bundle';
    }
}
