<?php


namespace Core\MultimediaBundle\Manager;


interface ConfigurationManagerInterface
{
    /**
     * Get named config
     *
     * @param string $className
     * @param string $configName
     *
     * @return mixed
     */
    public function getConfig(string $className, string $configName);
}