<?php


namespace Core\MultimediaBundle\Manager;


use Core\MultimediaBundle\Configuration;
use Core\MultimediaBundle\Loader\LoaderInterface;

class ConfigurationManager implements ConfigurationManagerInterface
{
    private $configs;

    public function __construct(LoaderInterface $uploadConfigLoader)
    {
        $this->configs = $uploadConfigLoader->getMappings();
    }

    /**
     * Get named config
     *
     * @param string $className
     * @param string $configName
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function getConfig(string $className, string $configName)
    {
        if (array_key_exists($className, $this->configs)) {

            $classConfig = $this->configs[$className];

            if (array_key_exists($configName, $classConfig)) {
                return new Configuration($classConfig[$configName]);
            }

            throw new \Exception("Unknown configuration name `$configName`");
        }

        throw new \Exception("There is no class name in configuration `$className`");
    }
}