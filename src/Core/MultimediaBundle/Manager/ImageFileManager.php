<?php


namespace Core\MultimediaBundle\Manager;


use AppBundle\Service\SystemService;
use Core\MultimediaBundle\Configuration;
use Core\MultimediaBundle\ImagePackage;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ImageFileManager
{
    const DEFAULT_EXTENSION = '.jpg';

    const IMAGE_SIZE_DELIMITER = 'x';

    /**
     * @var string
     */
    private $defaultExtension = self::DEFAULT_EXTENSION;

    /**
     * @var string
     */
    private $resourcesDir;

    /**
     * @var ConfigurationManagerInterface
     */
    private $configManager;

    /**
     * @var ImageManager
     */
    private $imageManager;

    /**
     * @var SystemService
     */
    private $systemService;

    /**
     * @var string
     */
    private $configs;

    /**
     * ImageLoader constructor.
     *
     * @param ConfigurationManagerInterface $configManager
     * @param ImageManager $imageManager
     * @param SystemService $systemService
     * @param string $resourcesDir
     */
    public function __construct(
        ConfigurationManagerInterface $configManager,
        ImageManager $imageManager,
        SystemService $systemService,
        string $resourcesDir
    ) {
        $this->configManager = $configManager;
        $this->imageManager = $imageManager;
        $this->systemService = $systemService;
        $this->resourcesDir = $resourcesDir;
    }

    /**
     * @return string
     */
    public function getDefaultExtension()
    {
        return $this->defaultExtension;
    }

    /**
     * @param string $defaultExtension
     */
    public function setDefaultExtension($defaultExtension)
    {
        $this->defaultExtension = $defaultExtension;
    }


    public function getImageSizeString(Image $image)
    {
        return $image->getWidth() . self::IMAGE_SIZE_DELIMITER . $image->getHeight();
    }

    /**
     * @param string|null $extension
     * @param string|null $postfix
     *
     * @return string
     */
    public function generateFileName(string $extension = null, string $postfix = null)
    {

        $filename = $this->systemService->getUniqueString();

        if ($postfix) {
            $filename .= "_$postfix";
        }
        $filename .= ($extension)
            ? $extension
            : $this->getDefaultExtension();

        return $filename;
    }

    private function getImageManagerDriverName()
    {
        return $this->imageManager->config['driver'];
    }

    public function isInterventionMethodExists(string $methodName)
    {
        if(class_exists(
            sprintf(
                '\Intervention\Image\%s\Commands\%sCommand',
                ucfirst($this->getImageManagerDriverName()),
                ucfirst($methodName)
            )
        )) {
            return true;
        }

        return false;
    }

    public function makeImage(string $pathname): Image
    {
        return $this->imageManager->make($pathname);
    }

    private function applyInterventionMethodsToImage(&$image, $methodsCalls)
    {
        foreach ($methodsCalls as $method => $parameters) {
            $parsedParameters = $this->parseInterventionMethodParameters($parameters);

            if ($this->isInterventionMethodExists($method)) {
                $image = call_user_func_array([$image, $method], $parsedParameters);
            }
        }
    }

    private function parseInterventionMethodParameters($parameters)
    {
        return array_map(function ($parameter) {
            if (is_array($parameter)) {
                if (!array_key_exists('constraints', $parameter)) {
                    return null;
                }

                return function ($constraint) use ($parameter) {
                    foreach ($parameter['constraints'] as $constraintCall) {
                        $constraint->$constraintCall();
                    }
                };

            }

            return $parameter;
        }, $parameters);
    }

    private function encodeImagePackageData($data)
    {
        if (is_null($data)) {
            return null;
        }

        if (is_array($data)) {
            return array_map(function (ImagePackage $imagePackage) {
                return $imagePackage->toArray();
            }, $data);
        } else {
            $expectedClassName = get_class($data);
            if ($expectedClassName != ImagePackage::class) {
                throw new \Exception("Argument must be an instance of `$expectedClassName` class");
            }

            return $data->toArray();
        }
    }

    public function fillEntityProperty($entity, string $propertyName, $data)
    {
        $setter = $this->systemService->getSetterFunctionName($propertyName);

        if (!method_exists($entity, $setter)) {
            throw new \Exception('Entity ' . get_class($entity) . ' doesn\'t have method ' . $setter);
        }

        call_user_func_array([$entity, $setter], [$data]);
    }

    // TODO: unit test
    public function fetchImagePackageFromEntity($entity, string $propertyName)
    {
        $getter = $this->systemService->getGetterFunctionName($propertyName);

        if (!method_exists($entity, $getter)) {
            throw new \Exception('Entity ' . get_class($entity) . ' doesn\'t have method ' . $getter);
        }

        $encodedData = call_user_func_array([$entity, $getter], []);

        if (!$encodedData) {
            return null;
        }

        return $this->createImagePackageFromEncodedData($encodedData);

    }

    private function createImagePackageFromEncodedData($encodedData)
    {
        if ($this->isEncodedDataArray($encodedData)) {
            $imagePackages = [];

            foreach ($encodedData as $data) {
                $imagePackages []= $this->createImagePackage($data);
            }

            return $imagePackages;
        } else {
            return $this->createImagePackage($encodedData);
        }
    }

    private function isEncodedDataArray($encodedData)
    {
        return !array_key_exists('filenames', $encodedData);
    }


    private function createImagePackage($data)
    {
        if (!$data) {
            return;
        }
        return ImagePackage::create($data);
    }
    private function createUploadDirIfNotExists(string $uploadDir)
    {
        if (!file_exists($this->resourcesDir . $uploadDir)) {
            mkdir($this->resourcesDir . $uploadDir, 0775);
        }
    }

    private function getFullFilePath(string $uploadDir, string $filename): string
    {
        return $this->resourcesDir . $uploadDir . "/$filename";
    }

    public function uploadSingle(
        UploadedFile $file,
        $filesConfig,
        $uploadDir
    ) {
        
        $imagePackage = new ImagePackage();

        // Foreach files config upload file
        
        foreach ($filesConfig as $configKey => $fileConfig) {
            
            $image = $this->makeImage($file->getPathname());

            // TODO: unit tests
            if (isset($fileConfig) && array_key_exists('intervention', $fileConfig)) {
                $this->applyInterventionMethodsToImage($image, $fileConfig['intervention']);
            }

            $filename = $this->generateFileName(
                @$filesConfig['extension'],
                $this->getImageSizeString($image)
            );

            $imagePackage->addFilename($filename, $configKey);

            $fullPath = $this->getFullFilePath($uploadDir, $filename);

            $this->createUploadDirIfNotExists($uploadDir);

            $image->save($fullPath, 100);
        }

        // Return image package associated with this configuration
        return $imagePackage;
    }

    public function uploadCollection(
        $files,
        $filesConfig,
        $uploadDir
    ) {
        return array_map(
            function (UploadedFile $file) use ($filesConfig, $uploadDir) {
                return $this->uploadSingle($file, $filesConfig, $uploadDir);
            },
            $files
        );
    }

    public function upload(
        $entity,
        $fileData,
        $configName
    ) {
        if (!$entity || !$fileData || !$configName) {
            throw new \Exception('Not enough data for image uploading');
        }

        // Fetch configuration
        /**
         * @var $currentConfig Configuration
         */
        $currentConfig = $this->configManager->getConfig(get_class($entity), $configName);

        $uploadDir = $currentConfig->getUploadDir();
        $filesConfig = $currentConfig->getFilesConfig();

        if (is_array($fileData)) {
            $imagePackageData = $this->uploadCollection($fileData, $filesConfig, $uploadDir);
        } else {
            $imagePackageData = $this->uploadSingle($fileData, $filesConfig, $uploadDir);
        }

        return $imagePackageData;
    }

    public function fillEntityImagePackageData($entity, $imagePackageData, $configName, $append = false)
    {
        /**
         * @var $currentConfig Configuration
         */
        $currentConfig = $this->configManager->getConfig(get_class($entity), $configName);
        $property = $currentConfig->getProperty();

        if ($append) {
            $existingImagePackageData = (array) $this->fetchImagePackageFromEntity($entity, $property);
            $imagePackageData = array_merge($existingImagePackageData, $imagePackageData);
        }

        $encodedImagePackageData = $this->encodeImagePackageData($imagePackageData);

        $this->fillEntityProperty(
            $entity,
            $property,
            $encodedImagePackageData
        );

        return $imagePackageData;
    }

    public function getEntityImagePackageData($entity, $configName)
    {
        if (!$entity || !$configName) {
            throw new \Exception('Not enough data for image removing');
        }

        /**
         * @var $currentConfig Configuration
         */
        $currentConfig = $this->configManager->getConfig(get_class($entity), $configName);
        $property = $currentConfig->getProperty();

        return $this->fetchImagePackageFromEntity($entity, $property);
    }

    public function remove($entity, $configName)
    {

        if (!$entity || !$configName) {
            throw new \Exception('Not enough data for image removing');
        }

        /**
         * @var $currentConfig Configuration
         */
        $currentConfig = $this->configManager->getConfig(get_class($entity), $configName);
        $property = $currentConfig->getProperty();
        $uploadDir = $currentConfig->getUploadDir();

        $imagePackageData = $this->fetchImagePackageFromEntity($entity, $property);

        $this->removeFilesFromImagePackageData($imagePackageData, $uploadDir);

        return true;
    }

    public function removeOne($entity, $configName, $index)
    {
        if (!$entity || !$configName) {
            throw new \Exception('Not enough data for image removing');
        }

        /**
         * @var $currentConfig Configuration
         */
        $currentConfig = $this->configManager->getConfig(get_class($entity), $configName);
        $property = $currentConfig->getProperty();
        $uploadDir = $currentConfig->getUploadDir();
        
        $imagePackageData = $this->fetchImagePackageFromEntity($entity, $property);

        $this->removeImagePackageFiles($imagePackageData[$index], $uploadDir);

        array_splice($imagePackageData, $index, 1);

        return $imagePackageData;


    }

    private function removeImagePackageFiles(ImagePackage $imagePackage, string $uploadDir)
    {
        $filenames = $imagePackage->getFilenames();

        array_walk(
            $filenames,
            function ($filename) use ($uploadDir) {
                $fullpath = $this->getFullFilePath($uploadDir, $filename);

                if (file_exists($fullpath) && !unlink($fullpath)) {
                    throw new UnprocessableEntityHttpException('Unable to remove ' . $filename . ' image');
                }
            }
        );
    }

    private function removeFilesFromImagePackageData($imagePackageData, string $uploadDir)
    {
        if (!$imagePackageData) {
            return false;
        }

        if (is_array($imagePackageData)) {
            array_walk($imagePackageData, function (ImagePackage $imagePackage) use ($uploadDir) {
                $this->removeImagePackageFiles($imagePackage, $uploadDir);
            });
        } else {
            $this->removeImagePackageFiles($imagePackageData, $uploadDir);
        }
    }
    
    
}