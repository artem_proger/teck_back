<?php

namespace Core\MultimediaBundle;

class ImagePackage
{

    private $filenames = [];

    private $meta;

    public function __construct($filenames = null, $meta = null)
    {
        $this->filenames = $filenames;
        $this->meta = $meta;
    }

    public function getFilename($configKey)
    {
        return $this->filenames[$configKey];
    }

    public function addFilename($filename, $configKey)
    {
        $this->filenames[$configKey] = $filename;
    }

    public function getFilenames()
    {
        return $this->filenames;
    }

    public function setMeta($meta)
    {
        $this->meta = $meta;
    }

    public function getMeta()
    {
        return $this->meta;
    }

    public function toArray()
    {
        $data = [
            'filenames' => $this->filenames
        ];

        if (isset($this->meta)) {
            $data['meta'] = $this->meta;
        }

        return $data;
    }

    public static function create($data)
    {
        return new self($data['filenames'], @$data['meta']);
    }
}