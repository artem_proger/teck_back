<?php

namespace Core\MultimediaBundle\Loader;

use CatalogBundle\CatalogBundle;
use Core\MultimediaBundle\MultimediaBundle;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;
use Symfony\Component\Yaml\Yaml;

class UploadConfigLoader implements LoaderInterface, CacheWarmerInterface
{
    /**
     * @var $cache ConfigCache
     */
    private $cache;

    /**
     * @var $yamlParser Yaml
     */
    private $yamlParser;

    /**
     * @var array
     */
    private $bundlesCollection;

    /**
     * UploadConfigLoader constructor.
     *
     * @param $cacheDir
     * @param $bundlesCollection
     * @param $isDebug
     * @internal param $bundles
     */
    public function __construct($cacheDir, $bundlesCollection, $isDebug)
    {
        $this->cache = new ConfigCache(sprintf('%s/uploads.cache.php', $cacheDir), $isDebug);
        $this->bundlesCollection = $bundlesCollection;
        $this->yamlParser = new Yaml();
    }

    /**
     * @return mixed
     */
    public function getMappings()
    {
        if (!$this->cache->isFresh()) {
            $this->warmUp(null);
        }

        return require $this->cache->getPath();
    }


    /**
     * {@inheritdoc}
     */
    public function isOptional()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function warmUp($cacheDir)
    {
        $config = [];
        $resources = [];

        foreach ($this->bundlesCollection as $bundleClass) {
            $ref = new \ReflectionClass($bundleClass);
            $bundleNamespace = $ref->getNamespaceName();
            $entityNamespacePart = '\\Entity';

            $configDir = dirname($ref->getFileName()) . '/Resources/config/upload';
            if (file_exists($configDir)) {
                $uploadDirFiles = array_diff(scandir($configDir), ['..', '.']);

                foreach ($uploadDirFiles as $filename) {
                    $entityName = str_replace('.yml', '', $filename);
                    $configPath = $configDir . "/$filename";
                    $fullClassName = $bundleNamespace . $entityNamespacePart . '\\' . $entityName;
                    $config[$fullClassName] = $this->yamlParser->parse(file_get_contents($configPath));
                    $resources[] = new FileResource($configPath);

                }
            }
        }

        $this->cache->write(
            sprintf('<?php return %s;', var_export($config, true)),
            $resources
        );
    }
}