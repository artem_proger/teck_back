<?php

namespace Core\MultimediaBundle\Loader;


interface LoaderInterface
{
    /**
     * Get loader config
     * 
     * @return mixed
     */
    public function getMappings();
}