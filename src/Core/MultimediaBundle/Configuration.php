<?php


namespace Core\MultimediaBundle;

/**
 * Class Configuration
 *
 * One of upload.yml named configuration wrapper
 *
 * @package Core\MultimediaBundle
 */
class Configuration
{
    const FILES_KEY = 'files';
    const UPLOAD_DIR_KEY = 'upload_dir';
    const PROPERTY_KEY = 'property';

    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getFilesConfig()
    {
        return $this->config[self::FILES_KEY];
    }

    public function getUploadDir()
    {
        return $this->config[self::UPLOAD_DIR_KEY];
    }

    public function getProperty()
    {
        return $this->config[self::PROPERTY_KEY];
    }
}