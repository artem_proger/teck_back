<?php


namespace Core\FilterRepoBundle\Filter;

abstract class DQL_Filter
{
    /**
     * @var string
     */
    private $alias;

    /**
     * @var array
     */
    private $parameters;

    public function __construct(string $alias, $parameters = null)
    {
        $this->alias = $alias;
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasParameter(string $key) {
        return isset($this->parameters[$key]);
    }

    /**
     * @param string $key
     *
     * @return mixed|null
     */
    public function getParameter(string $key) {

        if (!array_key_exists($key, $this->parameters)) {
            return null;
        }

        return $this->parameters[$key];
    }

    /**
     * @return array|null
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    abstract public function getCondition(): string;
}