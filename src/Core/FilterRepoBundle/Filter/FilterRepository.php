<?php


namespace Core\FilterRepoBundle\Filter;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

abstract class FilterRepository extends EntityRepository
{

    private $dqlFilters = [];

    protected $fetchJoinCollection = false;
    protected $maxResults = 9;

    abstract function getAlias(): string;

    public static function getCurrentPage()
    {
        return array_key_exists('page', $_GET) ? (int) $_GET['page'] : 1;
    }
    
    public function getDqlWithAlias()
    {
        return $this->createQueryBuilder($this->getAlias())->getDQL();
    }

    public function getDqlSummary()
    {
        $selectDql = $this->getDqlWithAlias();
        return $this->applyFiltersToDqlQuery($selectDql);
    }

    public function getSummaryQuery()
    {
        return $this->getEntityManager()
            ->createQuery(
                $this->getDqlSummary()
            );
    }

    public function paginateAll()
    {
        $page = self::getCurrentPage();
        $firstResult = ($page - 1) * $this->maxResults;

        $query = $this->getSummaryQuery()
            ->setFirstResult($firstResult)
            ->setMaxResults($this->maxResults);

        $paginator = new Paginator(
            $query,
            $this->fetchJoinCollection
        );

        return $paginator;
    }

    public function findAll()
    {
        return $this->getSummaryQuery()->getResult();
    }

    public function getFilters()
    {
        return $this->dqlFilters;
    }

    public function addDqlFilter($filterClass, $parameters = null)
    {
        $this->dqlFilters []= new $filterClass($this->getAlias(), $parameters);
    }

    public function getDqlFilterCondition(DQL_Filter $filter)
    {
        return $filter->getCondition();
    }

    protected function applyFiltersToDqlQuery(string $dql)
    {
        $dqlNotEmptyConditions = $this->getFiltersNotEmptyConditions($this->dqlFilters);

        if (count($dqlNotEmptyConditions) == 0) {
            return $dql;
        }

        $dqlConditions = $this->getFilterConditions($dqlNotEmptyConditions);

        $dql .= ' WHERE ';

        $dql .= implode(' AND ', $dqlConditions);


        return $dql;
    }

    protected function getFilterConditions($filters)
    {
        $conditions = [];

        foreach ($filters as $filter) {
            $condition = $this->getDqlFilterCondition($filter);
            $conditions []= '(' . $condition . ')';
        }

        return $conditions;
    }

    protected function getFiltersNotEmptyConditions($filters)
    {
        return array_filter($filters, function (DQL_Filter $filter) {
            return $filter->getCondition() != '';
        });
    }


}