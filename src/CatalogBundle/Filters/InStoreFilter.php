<?php


namespace CatalogBundle\Filters;


use Core\FilterRepoBundle\Filter\DQL_Filter;

class InStoreFilter extends DQL_Filter
{

    public function getCondition(): string
    {
        if (!$this->hasParameter('in_store') || $this->getParameter('in_store') == '') {
            return '';
        }

        $query = $this->getAlias() . '.inStore = ' . $this->getParameter('in_store');

        return $query;
    }
}