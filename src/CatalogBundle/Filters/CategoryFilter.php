<?php

namespace CatalogBundle\Filters;


use Core\FilterRepoBundle\Filter\DQL_Filter;

class CategoryFilter extends DQL_Filter
{

    public function getCondition(): string
    {
        $categoryId = $this->getParameter('category-id');

        if (!$this->hasParameter('category-id')) {
            return '';
        }

        if ($categoryId === '') {
            $categoryId = 0;
        }

        return $this->getAlias() . '.category = ' . $categoryId;
    }
}