<?php


namespace CatalogBundle\Filters;


use Core\FilterRepoBundle\Filter\DQL_Filter;

class TitleFilter extends DQL_Filter
{

    public function getCondition(): string
    {
        $titleSubstring = $this->getParameter('title');
        $titleQuery = str_replace(' ', '%', $titleSubstring);

        if (!$this->hasParameter('title') || $this->getParameter('title') == '') {
            return '';
        }

        return $this->getAlias() . '.title LIKE \'%' . $titleQuery . '%\'';
    }
}