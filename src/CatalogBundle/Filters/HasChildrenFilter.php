<?php


namespace CatalogBundle\Filters;


use Core\FilterRepoBundle\Filter\DQL_Filter;

class HasChildrenFilter extends DQL_Filter
{
    public function getCondition(): string
    {
        if (!$this->hasParameter('has-children') || $this->getParameter('has-children') != false) {
            return '';
        }

        return '(SELECT COUNT(c1) FROM CatalogBundle:Category c1 WHERE c1.parent = ' . $this->getAlias() . ') = 0';
    }
}