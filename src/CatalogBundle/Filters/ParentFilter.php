<?php


namespace CatalogBundle\Filters;

use Core\FilterRepoBundle\Filter\DQL_Filter;

class ParentFilter extends DQL_Filter
{
    public function getCondition(): string
    {

        if (!$this->hasParameter('parent_id')) {
            return '';
        }
        $parentId = $this->getParameter('parent_id');

        if ($parentId > 0) {
            return $this->getAlias() . '.parent = ' . $parentId;
        } else {
            return $this->getAlias() . '.parent IS NULL ';
        }
    }
}