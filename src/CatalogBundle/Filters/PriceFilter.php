<?php


namespace CatalogBundle\Filters;

use Core\FilterRepoBundle\Filter\DQL_Filter;

class PriceFilter extends DQL_Filter
{

    const PRICE_FROM_COL = 'priceFrom';
    const PRICE_TO_COL = 'priceTo';

    public function getCondition(): string
    {
        $priceFrom = $this->getParameter('price_from');
        $priceTo = $this->getParameter('price_to');

        if (!$this->hasParameter('price_from')
            || !$this->hasParameter('price_to')
            || empty($priceFrom)
            || empty($priceTo)
        ) {
            return '';
        } else if ($this->getParameter('price_from') > $this->getParameter('price_to')) {
            return '1 = 0';
        }

        $priceFrom = $this->getParameter('price_from');
        $priceTo = $this->getParameter('price_to');

        $query = $this->getAlias() . '.' . self::PRICE_FROM_COL . ' <= ' . $priceTo .
            ' AND ' . $this->getAlias() . '.' . self::PRICE_TO_COL . ' >= ' . $priceFrom;

        return $query;
    }
}