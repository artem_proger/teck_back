<?php


namespace CatalogBundle\Entity;

class PriceRate
{
    private $id;
    protected $product;
    
    protected $range;
    protected $price;
    protected $postfix;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set range
     *
     * @param string $range
     *
     * @return PriceRate
     */
    public function setRange($range)
    {
        $this->range = $range;

        return $this;
    }

    /**
     * Get range
     *
     * @return string
     */
    public function getRange()
    {
        return $this->range;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return PriceRate
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set postfix
     *
     * @param string $postfix
     *
     * @return PriceRate
     */
    public function setPostfix($postfix)
    {
        $this->postfix = $postfix;

        return $this;
    }

    /**
     * Get postfix
     *
     * @return string
     */
    public function getPostfix()
    {
        return $this->postfix;
    }

    /**
     * Set product
     *
     * @param \CatalogBundle\Entity\Product $product
     *
     * @return PriceRate
     */
    public function setProduct(\CatalogBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \CatalogBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
