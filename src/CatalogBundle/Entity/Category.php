<?php

namespace CatalogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Category
{
    private $id;
    private $parent;
    private $children;
    private $products;

    private $title;

    private $thumbnail;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add child
     *
     * @param \CatalogBundle\Entity\Category $child
     *
     * @return Category
     */
    public function addChild(\CatalogBundle\Entity\Category $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \CatalogBundle\Entity\Category $child
     */
    public function removeChild(\CatalogBundle\Entity\Category $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \CatalogBundle\Entity\Category $parent
     *
     * @return Category
     */
    public function setParent(\CatalogBundle\Entity\Category $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \CatalogBundle\Entity\Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set thumbnail
     *
     * @param array $thumbnail
     *
     * @return Category
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return array
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }
}
