<?php


namespace CatalogBundle\Entity;


use AppBundle\AcceptableTrait;
use Doctrine\Common\Collections\ArrayCollection;

class Product
{
    use AcceptableTrait;

    private $id;
    protected $priceRates;

    protected $title;
    protected $description;
    protected $category;
    protected $inStore;
    protected $gallery;
    protected $selectedImage;
    protected $priceFrom;
    protected $priceTo;

    public function __construct()
    {
        $this->priceRates = new ArrayCollection();
        $this->gallery = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set inStore
     *
     * @param boolean $inStore
     *
     * @return Product
     */
    public function setInStore($inStore)
    {
        $this->inStore = $inStore;

        return $this;
    }

    /**
     * Get inStore
     *
     * @return boolean
     */
    public function getInStore()
    {
        return $this->inStore;
    }

    /**
     * Set gallery
     *
     * @param json $gallery
     *
     * @return Product
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return json
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Add priceRate
     *
     * @param \CatalogBundle\Entity\PriceRate $priceRate
     *
     * @return Product
     */
    public function addPriceRate(\CatalogBundle\Entity\PriceRate $priceRate)
    {
        $this->priceRates[] = $priceRate;
        $priceRate->setProduct($this);

        return $this;
    }

    /**
     * Remove priceRate
     *
     * @param \CatalogBundle\Entity\PriceRate $priceRate
     */
    public function removePriceRate(\CatalogBundle\Entity\PriceRate $priceRate)
    {
        $this->priceRates->removeElement($priceRate);
    }

    /**
     * Get priceRates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPriceRates()
    {
        return $this->priceRates;
    }

    /**
     * Set category
     *
     * @param \CatalogBundle\Entity\Category $category
     *
     * @return Product
     */
    public function setCategory(\CatalogBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \CatalogBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set selectedImage
     *
     * @param string $selectedImage
     *
     * @return Product
     */
    public function setSelectedImage($selectedImage)
    {
        $this->selectedImage = $selectedImage;

        return $this;
    }

    /**
     * Get selectedImage
     *
     * @return string
     */
    public function getSelectedImage()
    {
        return $this->selectedImage;
    }

    /**
     * On post load
     */
    public function setDefaultGalleryValue()
    {
        if (is_null($this->gallery)) {
            $this->gallery = [];
        }
    }

    /**
     * Set priceFrom.
     *
     * @param float $priceFrom
     *
     * @return Product
     */
    public function setPriceFrom($priceFrom)
    {
        $this->priceFrom = $priceFrom;

        return $this;
    }

    /**
     * Get priceFrom.
     *
     * @return float
     */
    public function getPriceFrom()
    {
        return $this->priceFrom;
    }

    /**
     * Set priceTo.
     *
     * @param float $priceTo
     *
     * @return Product
     */
    public function setPriceTo($priceTo)
    {
        $this->priceTo = $priceTo;

        return $this;
    }

    /**
     * Get priceTo.
     *
     * @return float
     */
    public function getPriceTo()
    {
        return $this->priceTo;
    }
}
