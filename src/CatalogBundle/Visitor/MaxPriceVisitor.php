<?php


namespace CatalogBundle\Visitor;

use AppBundle\VisitorInterface;
use CatalogBundle\Entity\Product;

class MaxPriceVisitor implements VisitorInterface
{

    /**
     * @param $product Product
     * 
     * @return int
     */
    public function visit($product)
    {
        $priceRates = $product->getPriceRates();

        $maxPrice = 0;

        foreach ($priceRates as $priceRate) {
            $price = $priceRate->getPrice();

            if ($price > $maxPrice) {
                $maxPrice = $price;
            }
        }

        return $maxPrice;
    }
}