<?php


namespace CatalogBundle\Visitor;


use AppBundle\VisitorInterface;
use CatalogBundle\Entity\PriceRate;
use CatalogBundle\Entity\Product;

class MinPriceVisitor implements VisitorInterface
{

    /**
     * @param $product Product
     * 
     * @return mixed
     */
    public function visit($product)
    {
        $priceRates = $product->getPriceRates();

        $prices = array_map(function (PriceRate $priceRate) {
            return $priceRate->getPrice();
        }, $priceRates->toArray());

        return min($prices);
    }
}