<?php


namespace CatalogBundle\Repository;


use Core\FilterRepoBundle\Filter\FilterRepository;

class ProductRepository extends FilterRepository
{
    function getAlias(): string
    {
        return 'p';
    }
}