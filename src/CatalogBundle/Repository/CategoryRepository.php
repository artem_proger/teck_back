<?php

namespace CatalogBundle\Repository;

use Core\FilterRepoBundle\Filter\FilterRepository;

class CategoryRepository extends FilterRepository
{
    function getAlias(): string
    {
        return 'c';
    }
}