<?php

namespace CatalogBundle\Form;

use CatalogBundle\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PriceRateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('range', TextType::class, [
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('price', NumberType::class, [
                'invalid_message' => 'price_rate.price.float'
            ])
            ->add('postfix', TextType::class, [
                'required' => false
            ])/*
            ->add('product_id', EntityType::class, [
                'class' => Product::class,
                'property_path' => 'product'
            ])*/;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CatalogBundle\Entity\PriceRate'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'catalogbundle_pricerate';
    }


}
