<?php


namespace CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class CategoryThumbnailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('thumbnail', FileType::class, [
            'constraints' => [
                new File([
                    'maxSize' => '1M',
                    'mimeTypes' => ['image/png', 'image/jpeg', 'image/jpg'],
                    'mimeTypesMessage' => 'category.thumbnail.mime'
                ])
            ]
        ]);
    }
}