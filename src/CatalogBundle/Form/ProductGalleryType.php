<?php


namespace CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class ProductGalleryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('gallery', FileType::class, [
                'multiple' => true,
                /*'constraints' => [
                    new File([
                        'maxSize' => '1M',
                        'mimeTypes' => ['image/png', 'image/jpeg', 'image/jpg'],
                        'mimeTypesMessage' => 'category.thumbnail.mime'
                    ])
                ]*/
            ])
            ->add('selected', IntegerType::class);
    }
}