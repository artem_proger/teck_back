<?php


namespace CatalogBundle\Controller;

use AppBundle\Service\SystemService;
use AppBundle\Traits\ControllerHelper;
use CatalogBundle\Entity\Category;
use CatalogBundle\Entity\Product;
use CatalogBundle\Filters\CategoryFilter;
use CatalogBundle\Filters\InStoreFilter;
use CatalogBundle\Filters\PriceFilter;
use CatalogBundle\Filters\TitleFilter;
use CatalogBundle\Form\ProductGalleryType;
use CatalogBundle\Form\ProductType;
use Core\FilterRepoBundle\Filter\FilterRepository;
use Core\MultimediaBundle\ImagePackage;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductController extends Controller
{
    use ControllerHelper;

    /**
     * @var SystemService
     */
    private $systemService;

    public function __construct(SystemService $systemService)
    {
        $this->systemService = $systemService;
    }

    public function createAction(Request $request)
    {
        $product = new Product();

        $data = $this->getJsonFromRequest($request);

        $form = $this->createForm(ProductType::class, $product);
        $form->submit($data);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->getApiResponse()->validationFailedResponse(
                $form, 404
            );
        }

        $this->fillProductByData($product, $data);


        /**
         * @var $entityManager EntityManager
         */
        $entityManager = $this->getDoctrine()->getManager();

        $category = $this->getCategoryReference(@$data['category_id']);

        if (!$category) {
            return $this->getApiResponse()->errorResponse(403);
        }

        $product->setCategory($category);

        $entityManager->persist($product);
        $entityManager->flush();

        return $this->getApiResponse()->entityResponse($product, Response::HTTP_CREATED);
    }

    /**
     * @ParamConverter("product", class="CatalogBundle:Product")
     *
     * @param Request $request
     * @param Product $product
     *
     * @return array|\FOS\RestBundle\View\View
     */
    public function updateAction(Request $request, Product $product)
    {
        /**
         * @var $entityManager EntityManager
         */
        $entityManager = $this->getDoctrine()->getManager();

        $data = $this->getJsonFromRequest($request);

        $form = $this->createForm(ProductType::class, $product);
        $form->submit($data);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->getApiResponse()->validationFailedResponse(
                $form, Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $this->fillProductByData($product, $data);

        $category = $this->getCategoryReference(@$data['category_id']);
        if (!$category) {
            return $this->getApiResponse()->errorResponse(403);
        }

        $product->setCategory($category);

        $entityManager->persist($product);
        $entityManager->flush();

        return $this->getApiResponse()->entityResponse($product, Response::HTTP_OK);
    }

    /**
     * @QueryParam(
     *     name="category-id",
     *     requirements="\d+",
     *     description="Category",
     *     nullable=true
     * )
     * @QueryParam(
     *     name="title",
     *     requirements="[a-zA-Zа-яА-я\s]+",
     *     description="Title",
     *     nullable=true
     * )
     * @QueryParam(
     *     name="price-from",
     *     requirements="([0-9]*[.])?[0-9]+",
     *     description="Price from",
     *     nullable=true
     * )
     * @QueryParam(
     *     name="price-to",
     *     requirements="([0-9]*[.])?[0-9]+",
     *     description="Price to",
     *     nullable=true
     * )
     * @QueryParam(
     *     name="in-store",
     *     requirements="[01]+",
     *     description="In store",
     *     nullable=true
     * )
     * @param ParamFetcher $paramFetcher
     *
     * @return \FOS\RestBundle\View\View
     */
    public function collectionAction(ParamFetcher $paramFetcher)
    {
        /**
         * @var $repository FilterRepository
         */
        $repository = $this->getDoctrine()->getRepository(Product::class);

        $categoryId = $paramFetcher->get('category-id');
        $titleSubstring = $paramFetcher->get('title');
        $priceFrom = $paramFetcher->get('price-from');
        $priceTo = $paramFetcher->get('price-to');
        $inStore = $paramFetcher->get('in-store');

        $repository->addDqlFilter(CategoryFilter::class, ['category-id' => $categoryId]);
        $repository->addDqlFilter(TitleFilter::class, ['title' => $titleSubstring]);
        $repository->addDqlFilter(InStoreFilter::class, ['in_store' => $inStore]);
        $repository->addDqlFilter(PriceFilter::class, [
            'price_from' => $priceFrom,
            'price_to' => $priceTo,
        ]);

        return $this->getApiResponse()->paginationResponse(
            $repository->paginateAll()
        );
    }

    /**
     * @ParamConverter("product", class="CatalogBundle:Product")
     *
     * @param Product $product
     *
     * @return \FOS\RestBundle\View\View
     */
    public function detailsAction(Product $product)
    {
        return $this->getApiResponse()->entityResponse($product);
    }

    /**
     * @ParamConverter("product", class="CatalogBundle:Product")
     *
     * @param Product $product
     * @return \FOS\RestBundle\View\View
     */
    public function removeAction(Product $product)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $this->getImageFileManager()
            ->remove($product, $this->getUploadConfigKey());

        $entityManager->remove($product);
        $entityManager->flush();

        return $this->getApiResponse()->successResponse(300);
    }

    /**
     * @ParamConverter("product", class="CatalogBundle:Product")
     *
     * @param Request $request
     * @param Product $product
     *
     * @return \FOS\RestBundle\View\View
     *
     * @throws \Exception
     */
    public function galleryUploadAction(Request $request, Product $product)
    {
        $uploadFiles = $request->files->get('gallery');
        $selectedImageIndex = ($request->get('selected')) ? $request->get('selected') : 0;

        if (!$this->checkFiles($uploadFiles)) {
            return $this->invalidFileErrorResponse();
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        $form = $this->createForm(ProductGalleryType::class);

        $form->submit($request->files->all());

        if ($form->isSubmitted() && $form->isValid()) {

            $imageFileManager = $this->getImageFileManager();
            $imagePackageData = $imageFileManager
                ->upload(
                    $product,
                    $uploadFiles,
                    $this->getUploadConfigKey()
                );

            $imagePackageData = $imageFileManager->fillEntityImagePackageData(
                $product, $imagePackageData, $this->getUploadConfigKey(), true
            );

            if (!array_key_exists($selectedImageIndex, $imagePackageData)) {
                $selectedImageIndex = 0;
            }

            /**
             * @var $selectedImagePackage ImagePackage
             */
            $selectedImagePackage = $imagePackageData[$selectedImageIndex];
            $selectedFileName = $selectedImagePackage->getFilenames();
            $product->setSelectedImage($selectedFileName);

            $entityManager->persist($product);
            $entityManager->flush();

            return $this->getApiResponse()->entityResponse($product);
        }
        
        return $this->getApiResponse()->validationFailedResponse($form, 401);
    }

    /**
     * @ParamConverter("product", class="CatalogBundle:Product")
     *
     * @param Product $product
     *
     * @param $index
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function galleryRemoveOne(Product $product, $index)
    {
        $imageFileManager = $this->getImageFileManager();
        $imagePackageData = $imageFileManager->getEntityImagePackageData(
            $product, $this->getUploadConfigKey()
        );

        if (!array_key_exists($index, $imagePackageData)) {
            $this->getApiResponse()->errorResponse(402);
        }

        /**
         * @var $deletedImagePackage ImagePackage
         */
        $deletedImagePackage = $imagePackageData[$index];


        $imagePackageData = $imageFileManager->removeOne(
            $product, $this->getUploadConfigKey(), $index
        );

        if ($product->getSelectedImage() == $deletedImagePackage->getFilenames()) {
            /**
             * @var $firstImagePackageData ImagePackage
             */
            $firstImagePackageData = $imagePackageData[0];
            $product->setSelectedImage($firstImagePackageData->getFilenames());
        }

        $imageFileManager->fillEntityImagePackageData(
            $product, $imagePackageData, $this->getUploadConfigKey()
        );

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        return $this->getApiResponse()->entityResponse($product);
    }

    /**
     * @ParamConverter("product", class="CatalogBundle:Product")
     *
     * @param Product $product
     *
     * @return \FOS\RestBundle\View\View
     */
    public function galleryRemoveAll(Product $product)
    {
        $imageFileManager = $this->getImageFileManager();
        $imageFileManager->remove($product, $this->getUploadConfigKey());
        $imageFileManager->fillEntityImagePackageData(
            $product, null, $this->getUploadConfigKey()
        );
        $product->setSelectedImage(null);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        return $this->getApiResponse()->entityResponse($product);
    }

    /**
     * @ParamConverter("product", class="CatalogBundle:Product")
     *
     * @param Request $request
     * @param Product $product
     *
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function galleryMove(Request $request, Product $product)
    {
        $data = $this->getJsonFromRequest($request);

        $sourceIndex = $data['source'];
        $targetIndex = $data['target'];

        $entityManager = $this->getDoctrine()->getManager();

        $imageFileManager = $this->getImageFileManager();
        $imagePackageData = $imageFileManager->getEntityImagePackageData(
            $product, $this->getUploadConfigKey()
        );

        $this->systemService->moveElement($imagePackageData, $sourceIndex, $targetIndex);

        $imageFileManager->fillEntityImagePackageData(
            $product, $imagePackageData, $this->getUploadConfigKey()
        );

        $entityManager->persist($product);
        $entityManager->flush();

        return $this->getApiResponse()->entityResponse($product);

    }

    /**
     * Select gallery main image
     *
     * @param Product $product
     * @param $index
     *
     * @return \FOS\RestBundle\View\View
     *
     * @throws \Exception
     */
    public function gallerySelect(Product $product, $index)
    {
        $imageFileManager = $this->getImageFileManager();
        $imagePackageData = $imageFileManager->getEntityImagePackageData(
            $product, $this->getUploadConfigKey()
        );

        /**
         * @var $selectedImagePackageData ImagePackage
         */
        $selectedImagePackageData = $imagePackageData[$index];
        $product->setSelectedImage($selectedImagePackageData->getFilenames());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        return $this->getApiResponse()->entityResponse($product);
    }

    private function fillProductByData(Product $product, $data)
    {
        $product->setTitle($data['title']);
        if (array_key_exists('description', $data)) {
            $product->setDescription($data['description']);
        } else {
            $product->setDescription('');
        }
        $product->setInStore(@$data['in_store']);
    }

    private function getCategoryReference($categoryId)
    {
        if (!is_null($categoryId)) {
            $category = $this->getEm()->getReference(Category::class, $categoryId);

            if ($category) {
                return $category;
            }
        }

        return null;
    }

    private function getUploadConfigKey()
    {
        return 'product_gallery';
    }
}