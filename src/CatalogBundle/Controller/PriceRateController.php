<?php


namespace CatalogBundle\Controller;


use AppBundle\Traits\ControllerHelper;
use CatalogBundle\Entity\PriceRate;
use CatalogBundle\Entity\Product;
use CatalogBundle\Event\ChangePriceRatesEvent;
use CatalogBundle\Form\PriceRateType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;


class PriceRateController extends Controller
{
    use ControllerHelper;

    /**
     * @ParamConverter("product", class="CatalogBundle:Product", options={"id" = "product_id"})
     *
     * @param Request $request
     * @param Product $product
     *
     * @return array|\FOS\RestBundle\View\View
     */
    public function createAction(Request $request, Product $product)
    {
        $priceRate = new PriceRate();

        $data = $this->getJsonFromRequest($request);

        $form = $this->createForm(PriceRateType::class, $priceRate);
        $form->submit($data);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->getApiResponse()->validationFailedResponse(
                $form, 600
            );
        }

        $this->fillPriceRateByData($priceRate, $data);

        $product->addPriceRate($priceRate);

        /**
         * @var $dispatcher EventDispatcherInterface
         */
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(ChangePriceRatesEvent::NAME, new ChangePriceRatesEvent($product));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($priceRate);
        $entityManager->flush();

        return $this->getApiResponse()->entityResponse($priceRate, Response::HTTP_CREATED);
    }

    /**
     * @ParamConverter("priceRate", class="CatalogBundle:PriceRate", options={"id" = "price_rate_id"})
     *
     * @param Request $request
     * @param $product_id
     * @param PriceRate $priceRate
     *
     * @return array|\FOS\RestBundle\View\View
     */
    public function updateAction(Request $request, $product_id, PriceRate $priceRate)
    {
        /**
         * @var $entityManager EntityManager
         */
        $entityManager = $this->getDoctrine()->getManager();

        $data = $this->getJsonFromRequest($request);

        $form = $this->createForm(PriceRateType::class, $priceRate);
        $form->submit($data);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->getApiResponse()->validationFailedResponse(
                $form, Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $this->fillPriceRateByData($priceRate, $data);

        $entityManager->persist($priceRate);
        $entityManager->flush();

        /**
         * @var $product Product
         */
        $product = $entityManager->getRepository(Product::class)->find($product_id);
        /**
         * @var $dispatcher EventDispatcherInterface
         */
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(ChangePriceRatesEvent::NAME, new ChangePriceRatesEvent($product));

        $entityManager->persist($product);
        $entityManager->flush();

        return $this->getApiResponse()->entityResponse($priceRate, Response::HTTP_OK);
    }

    /**
     * @ParamConverter("product", class="CatalogBundle:Product", options={"id" = "product_id"})
     *
     * @param Product $product
     *
     * @return \FOS\RestBundle\View\View
     */
    public function collectionAction(Product $product)
    {
        $repository = $this->getDoctrine()->getRepository(PriceRate::class);

        return $this->getApiResponse()->collectionResponse(
            $repository->findByProduct($product)
        );

    }

    /**
     * @ParamConverter("priceRate", class="CatalogBundle:PriceRate", options={"id" = "price_rate_id"})
     *
     * @param $product_id
     * @param PriceRate $priceRate
     *
     * @return \FOS\RestBundle\View\View
     */
    public function removeAction($product_id, PriceRate $priceRate)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($priceRate);
        $entityManager->flush();

        /**
         * @var $product Product
         */
        $product = $entityManager->getRepository(Product::class)->find($product_id);
        /**
         * @var $dispatcher EventDispatcherInterface
         */
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(ChangePriceRatesEvent::NAME, new ChangePriceRatesEvent($product));
        $entityManager->persist($product);
        $entityManager->flush();

        return $this->getApiResponse()->successResponse(Response::HTTP_OK);
    }

    private function fillPriceRateByData(PriceRate $priceRate, $data)
    {
        $priceRate->setRange($data['range']);
        $priceRate->setPrice($data['price']);
        $priceRate->setPostfix(@$data['postfix']);
    }
}