<?php

namespace CatalogBundle\Controller;

use AppBundle\Traits\ControllerHelper;
use CatalogBundle\Entity\Category;
use CatalogBundle\Filters\HasChildrenFilter;
use CatalogBundle\Filters\ParentFilter;
use CatalogBundle\Form\CategoryThumbnailType;
use CatalogBundle\Form\CategoryType;
use CatalogBundle\Repository\CategoryRepository;
use Core\MultimediaBundle\Manager\ImageFileManager;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class CategoryController extends Controller
{
    use ControllerHelper;

    public function createAction(Request $request)
    {
        $category = new Category();

        $data = $this->getJsonFromRequest($request);
        
        $form = $this->createForm(CategoryType::class, $category);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {

            $category->setTitle($data['title']);

            $entityManager = $this->getDoctrine()->getManager();

            if (isset($data['parent_id'])) {
                $parentCategoryId = $data['parent_id'];
                $parentCategory = $entityManager->getReference(Category::class, $parentCategoryId);

                $category->setParent($parentCategory);
            }

            $entityManager->persist($category);
            $entityManager->flush();

            return $this->getApiResponse()->entityResponse($category, Response::HTTP_CREATED);
        }

        return $this->getApiResponse()->validationFailedResponse($form, 300);
    }

    /**
     * @ParamConverter("category", class="CatalogBundle:Category")
     *
     * @param Request $request
     * @param Category $category
     *
     * @return array|\FOS\RestBundle\View\View
     */
    public function updateAction(Request $request, Category $category)
    {
        $data = $this->getJsonFromRequest($request);

        $form = $this->createForm(CategoryType::class, $category);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $category->setTitle($data['title']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->getApiResponse()->entityResponse($category);
        }

        return $this->getApiResponse()->validationFailedResponse($form, 300);
    }

    /**
     * @QueryParam(name="parent-id", requirements="\d+", description="Parent Category", nullable=true)
     * @QueryParam(name="has-children", requirements="[0,1]+", description="Has children", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * 
     * @return \FOS\RestBundle\View\View
     */
    public function collectionAction(ParamFetcher $paramFetcher)
    {
        /**
         * @var $repository CategoryRepository
         */
        $repository = $this->getDoctrine()->getRepository(Category::class);

        $parentId = $paramFetcher->get('parent-id');
        $hasChildren = $paramFetcher->get('has-children');

        $repository->addDqlFilter(ParentFilter::class, ['parent_id' => $parentId]);
        $repository->addDqlFilter(HasChildrenFilter::class, ['has-children' => $hasChildren]);
        
        return $this->getApiResponse()->collectionResponse(
            $repository->findAll()
        );
    }

    /**
     * @ParamConverter("category", class="CatalogBundle:Category")
     *
     * @param Category $category
     *
     * @return \FOS\RestBundle\View\View
     */
    public function detailsAction(Category $category)
    {
        return $this->getApiResponse()->entityResponse($category);
    }

    /**
     * @ParamConverter("category", class="CatalogBundle:Category")
     *
     * @param Request $request
     * @param Category $category
     *
     * @return array|\FOS\RestBundle\View\View
     */
    public function assignAction(Request $request, Category $category)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $data = $this->getJsonFromRequest($request);

        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);

        if ($data['parent_id'] == null) {
            $category->setParent(null);
        } else {
            /**
             * @var $parentCategory Category
             */
            $parentCategory = $categoryRepository->find($data['parent_id']);

            if (!$parentCategory) {
                return $this->getApiResponse()->errorResponse(301, 404);
            }

            $category->setParent($parentCategory);
        }
        $entityManager->persist($category);
        $entityManager->flush();

        return $this->getApiResponse()->entityResponse($category);
    }

    /**
     * @ParamConverter("category", class="CatalogBundle:Category")
     *
     * @param Request $request
     * @param Category $category
     *
     * @return \FOS\RestBundle\View\View
     */
    public function thumbnailAction(Request $request, Category $category)
    {
        if (!$this->checkFile($request->files->get('thumbnail'))) {
            return $this->invalidFileErrorResponse();
        }

        $this->removeOldThumbnailFiles($category);

        $form = $this->createForm(CategoryThumbnailType::class);
        $form->submit($request->files->all());

        if ($form->isSubmitted() && $form->isValid()) {
            
            $imageFileManager = $this->getImageFileManager();

            $imagePackageData = $imageFileManager
                ->upload(
                    $category,
                    $request->files->get('thumbnail'),
                    $this->getUploadConfigKey()
                );

            $imageFileManager->fillEntityImagePackageData(
                $category, $imagePackageData, $this->getUploadConfigKey()
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->getApiResponse()->entityResponse($category);
        }


        return $this->getApiResponse()->validationFailedResponse($form, 302);

    }

    /**
     * @ParamConverter("category", class="CatalogBundle:Category")
     *
     * @param Category $category
     *
     * @return \FOS\RestBundle\View\View
     */
    public function removeAction(Category $category)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $this->removeOldThumbnailFiles($category);

        $entityManager->remove($category);
        $entityManager->flush();
        return $this->getApiResponse()->successResponse(200);
    }

    private function getUploadConfigKey()
    {
        return 'category_thumbnail';
    }

    private function removeOldThumbnailFiles(Category $category)
    {
        $this->getImageFileManager()
            ->remove(
                $category,
                $this->getUploadConfigKey()
            );
    }
}
