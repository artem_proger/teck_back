<?php


namespace CatalogBundle\EventListener;


use CatalogBundle\Event\ChangePriceRatesEvent;
use CatalogBundle\Visitor\MaxPriceVisitor;
use CatalogBundle\Visitor\MinPriceVisitor;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PriceFromToListener implements EventSubscriberInterface
{

    /**
     * Returns the events to which this class has subscribed.
     *
     * Return format:
     *     array(
     *         array('event' => 'the-event-name', 'method' => 'onEventName', 'class' => 'some-class', 'format' => 'json'),
     *         array(...),
     *     )
     *
     * The class may be omitted if the class wants to subscribe to events of all classes.
     * Same goes for the format key.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            ChangePriceRatesEvent::NAME => 'onChangePriceRates'
        ];
    }

    public function onChangePriceRates(ChangePriceRatesEvent $event)
    {
        $product = $event->getProduct();
        $priceFrom = $product->accept(new MinPriceVisitor());
        $priceTo = $product->accept(new MaxPriceVisitor());

        $product->setPriceFrom($priceFrom);
        $product->setPriceTo($priceTo);
    }
}