<?php


namespace CatalogBundle\Event;


use CatalogBundle\Entity\Product;
use Symfony\Component\EventDispatcher\Event;

class ChangePriceRatesEvent extends Event
{

    const NAME = 'catalog.event.change_price_rates';
    
    /**
     * @var Product
     */
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct()
    {
        return $this->product;
    }
}