<?php

$container->setParameter('app.messages', [
    \AppBundle\Service\MessageService::ERROR => include_once __DIR__ . '/error.php',
    \AppBundle\Service\MessageService::SUCCESS => include_once __DIR__ . '/success.php'
]);