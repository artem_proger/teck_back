<?php


namespace AppBundle\Command;


use AppBundle\PurgeTablesFixtureInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader as DataFixturesLoader;

class FixturePurger extends ContainerAwareCommand
{
    const PURGE_TABLES_FIXTURE_INTERFACE = 'AppBundle\PurgeTablesFixtureInterface';
    const DATA_FIXTURES_NAMESPACE_POSTFIX = '\\DataFixtures\\ORM\\';
    const DATA_FIXTURES_PATH_POSTFIX = '/DataFixtures/ORM';
    const SQL_FOREIGN_ON = 'SET FOREIGN_KEY_CHECKS=1';
    const SQL_FOREIGN_OFF = 'SET FOREIGN_KEY_CHECKS=0';
    const COMMAND_LOAD_FIXTURES = 'doctrine:fixtures:load';

    public function configure()
    {
        $this
            ->setName('app:fixtures:purge-load')
            ->setDescription('Truncate tables that used by Doctrine fixtures')
            ->setHelp('This command allows you to truncate tables that used by Doctrine fixtures');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var $kernel \Symfony\Component\HttpKernel\KernelInterface */
        $kernel = $this->getApplication()->getKernel();
        $paths = array($kernel->getRootDir().self::DATA_FIXTURES_PATH_POSTFIX);
        foreach ($kernel->getBundles() as $bundle) {
            $paths[] = $bundle->getPath().self::DATA_FIXTURES_PATH_POSTFIX;
        }

        $loader = new DataFixturesLoader($this->getContainer());

        foreach ($paths as $path) {
            if (is_dir($path)) {
                $loader->loadFromDirectory($path);
            } elseif (is_file($path)) {
                $loader->loadFromFile($path);
            }
        }

        $fixtures = $loader->getFixtures();

        foreach ($fixtures as $fixture) {
            $fixtureClass = get_class($fixture);
            $reflectionClass = new \ReflectionClass($fixtureClass);

            if (!$reflectionClass->implementsInterface(self::PURGE_TABLES_FIXTURE_INTERFACE)) {
                continue;
            }

            /**
             * @var $fixture PurgeTablesFixtureInterface
             */
            $entityClasses = $fixture->getEntityClasses();

            if (!count($entityClasses)) {
                continue;
            }

            /**
             * @var $em EntityManager
             */
            $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
            $connection = $em->getConnection();
            $connection->beginTransaction();

            $dbPlatform = $connection->getDatabasePlatform();

            $truncateTableQueries = [];
            $truncatedTables = [];
            foreach ($entityClasses as $entityClass) {
                $table = $em->getClassMetadata($entityClass)->getTableName();
                $truncatedTables []= $table;
                $truncateTableQueries []= $dbPlatform->getTruncateTableSQL($table);
            }

            $output->writeln([
                'Truncating tables ' . implode(', ', $truncatedTables) . '...',
            ]);

            $truncateQuery = implode(';', $truncateTableQueries) . ';';

            try {
                $connection->query(self::SQL_FOREIGN_OFF);
                $connection->query($truncateQuery);
                $connection->query(self::SQL_FOREIGN_ON);
                $output->writeln([
                    'Success'
                ]);
                $loadFixturesCommand = $this->getApplication()->find(self::COMMAND_LOAD_FIXTURES);
                $loadFixturesReturnCode = $this->getApplication()->run(new ArrayInput([
                    'command' => $loadFixturesCommand->getName(),
                    '--append' => true
                ]));

                if ($loadFixturesReturnCode !== 0) {
                    throw new \Exception('Loading fixtures has been failed');
                }
                $connection->commit();
            } catch (\Exception $e) {
                $connection->rollBack();
                $connection->query(self::SQL_FOREIGN_ON);
                $output->writeln([
                    'Error: ' . $e->getMessage()
                ]);
            }
        }
    }

}