<?php

namespace AppBundle\EventListener;

use AppBundle\Service\ApiResponseService;
use FOS\RestBundle\View\ViewHandler;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;

class ApiResponseEventListener implements EventSubscriberInterface
{
    /**
     * @var $viewHandler ViewHandler
     */
    private $viewHandler;

    /**
     * @var $apiResponseService ApiResponseService
     */
    private $apiResponseService;

    /**
     * ApiResponseEventListener constructor.
     *
     * @param ViewHandler $viewHandler
     * @param ApiResponseService $apiResponseService
     */
    public function __construct(ViewHandler $viewHandler, ApiResponseService $apiResponseService)
    {
        $this->viewHandler = $viewHandler;
        $this->apiResponseService = $apiResponseService;
    }

    /**
     * Get subscribed events
     * 
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
            FOSUserEvents::REGISTRATION_FAILURE => 'onFailedValidation',
            Events::AUTHENTICATION_SUCCESS => 'onAuthenticationSuccess',
            Events::AUTHENTICATION_FAILURE => 'onAuthenticationFailure'
        ];
    }
    
    public function onRegistrationSuccess(FormEvent $event)
    {
        $event->setResponse($this->viewHandler->handle(
            $this->apiResponseService->successResponse(100, Response::HTTP_CREATED)
        ));
    }

    public function onFailedValidation(FormEvent $event)
    {
        $errors = [];

        foreach ($event->getForm() as $key => $field) {
            $fieldErrors = iterator_to_array($field->getErrors(true));
            if (count($fieldErrors)) {
                array_walk($fieldErrors, function (FormError &$value) {
                    $value = $value->getMessage();
                });
                $errors[$key] = $fieldErrors;
            }
        };

        $event->setResponse($this->viewHandler->handle(
            $this->apiResponseService->errorResponse(100, Response::HTTP_BAD_REQUEST, $errors)
        ));
    }

    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        $event->setData([
            'token' => $event->getData()['token'],
            'refresh_token' => $event->getData()['refresh_token']
        ]);
    }

    public function onAuthenticationFailure(AuthenticationFailureEvent $event)
    {
        $event->setResponse($this->viewHandler->handle(
            $this->apiResponseService->errorResponse(200, Response::HTTP_UNAUTHORIZED)
        ));
    }
}