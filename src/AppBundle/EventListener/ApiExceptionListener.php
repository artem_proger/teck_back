<?php


namespace AppBundle\EventListener;


use AppBundle\Service\ApiResponseService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiExceptionListener
{
    /**
     * @var ApiResponseService
     */
    private $apiResponseService;

    public function __construct(ApiResponseService $apiResponseService)
    {
        $this->apiResponseService = $apiResponseService;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof HttpException) {
            switch ($exception->getStatusCode()) {
                case 404:
                    $view = $this->apiResponseService->errorResponse(102, 404);
                    $event->setResponse(
                        new JsonResponse(
                            $view->getData(),
                            $view->getStatusCode()
                        )
                    );
                    break;
                default:
                    break;
            }
        }
    }
}