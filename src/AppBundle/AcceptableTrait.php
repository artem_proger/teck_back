<?php

namespace AppBundle;


trait AcceptableTrait
{
    public function accept(VisitorInterface $visitor) {
        return $visitor->visit($this);
    }
}