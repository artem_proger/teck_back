<?php

namespace AppBundle\Traits;

use AppBundle\Service\ApiResponseService;
use Core\MultimediaBundle\Manager\ImageFileManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

trait ControllerHelper
{
    private function getJsonFromRequest(Request $request)
    {
        $decoded = json_decode($request->getContent(), true);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $decoded;
        } else {
            throw new BadRequestHttpException('Неверный формат тела запроса');
        }
    }

    /**
     * @return ApiResponseService
     */
    private function getApiResponse(): ApiResponseService
    {
        return $this->get('app.api_response');
    }

    private function checkFile(UploadedFile $file = null)
    {
        if (!isset($file)) {
            return false;
        }

        if ($file->isValid()) {
            return true;
        }

        return false;
    }

    private function checkFiles($files)
    {
        foreach ($files as $file) {
            if (!$this->checkFile($file)) {
                return false;
            }
        }

        return true;
    }

    private function getEm(): EntityManagerInterface
    {
        return $this->getDoctrine()->getManager();
    }

    private function invalidFileErrorResponse()
    {
        return $this->getApiResponse()->errorResponse(104);
    }

    private function getImageFileManager(): ImageFileManager
    {
        return $this->get('multimedia.image_file_manager');
    }
}