<?php


namespace AppBundle;


interface PurgeTablesFixtureInterface
{
    /**
     * Get entity classes which used in this fixture
     *
     * @return mixed
     */
    public function getEntityClasses(): array;
}