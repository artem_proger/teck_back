<?php


namespace AppBundle;


interface VisitorInterface
{
    public function visit($object);
}