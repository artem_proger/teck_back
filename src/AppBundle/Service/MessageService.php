<?php


namespace AppBundle\Service;


use Zend\Code\Exception\RuntimeException;

class MessageService
{
    const ERROR = 'error';
    const SUCCESS = 'success';
    
    /**
     * All messages
     *
     * @var array
     */
    private $messages;

    public function __construct(array $messages)
    {
        $this->messages = $messages;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param $code
     * @param $type
     * @return string
     */
    public function getMessage($code, $type) : string
    {
        if (!in_array($type, [self::ERROR, self::SUCCESS])) {
            throw new RuntimeException("Unknown message type '$type'");
        }
        
        if (!array_key_exists($code, $this->messages[$type])) {
            throw new RuntimeException("Unknown $type message code '$code'");
        }

        return $this->messages[$type][$code];
    }
}