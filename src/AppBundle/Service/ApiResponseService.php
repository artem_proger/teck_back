<?php

namespace AppBundle\Service;


use Core\FilterRepoBundle\Filter\FilterRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;

class ApiResponseService
{
    /**
     * @var $messageService MessageService
     */
    private $messageService;

    /**
     * ApiResponse constructor.
     * 
     * @param MessageService $messageService
     */
    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    /**
     * Error response
     *
     * @param int $code
     * @param int $status
     * @param null $errors
     * @return array|View
     */
    public function errorResponse(int $code, int $status = 400, $errors = null) : View
    {
        $responseStructure = [
            'status' => MessageService::ERROR,
            'code' => $code,
            'message' => $this->messageService->getMessage($code, MessageService::ERROR)
        ];

        if (isset($errors)) {
            $responseStructure['errors'] = $errors;
        }

        return View::create($responseStructure, $status);
    }

    /**
     * Success response
     *
     * @param int $code
     * @param int $status
     * @return View
     */
    public function successResponse($code, $status = 200) : View
    {
        return View::create([
            'status' => MessageService::SUCCESS,
            'code' => $code,
            'message' => $this->messageService->getMessage($code, MessageService::SUCCESS)
        ], $status);
    }

    /**
     * Entity response
     *
     * @param $data
     * @param $status
     *
     * @return View
     */
    public function entityResponse($data, $status = 200) : View
    {
        return View::create($data, $status);
    }

    /**
     * Collection of entities response
     *
     * @param $data
     * @param int $status
     *
     * @return View
     */
    public function collectionResponse($data, $status = 200) : View
    {
        return View::create([
            'data' => $data
        ], $status);
    }

    public function paginationResponse(Paginator $paginator)
    {
        $data = iterator_to_array($paginator);

        return View::create([
            'meta' => [
                'page' => FilterRepository::getCurrentPage(),
                'total' => $paginator->count(),
                'per_page' => $paginator->getQuery()->getMaxResults(),
                'count' => count($data)
            ],
            'data' => $data
        ]);
    }

    public function validationFailedResponse(FormInterface $form, $code, $status = Response::HTTP_BAD_REQUEST)
    {
        $errors = [];

        foreach ($form as $key => $field) {
            $fieldErrors = iterator_to_array($field->getErrors(true));
            if (count($fieldErrors)) {
                array_walk($fieldErrors, function (FormError &$value) {
                    $value = $value->getMessage();
                });
                $errors[$key] = $fieldErrors;
            }
        };

        return $this->errorResponse($code, $status, $errors);
    }
}