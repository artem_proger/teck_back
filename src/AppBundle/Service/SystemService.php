<?php


namespace AppBundle\Service;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SystemService
{

    /**
     * Get unique string
     *
     * @param string $prefix
     * @param bool $moreEntropy
     *
     * @return string
     */
    public function getUniqueString(string $prefix = "", bool $moreEntropy = false)
    {
        return uniqid($prefix, $moreEntropy);
    }

    /**
     * @param string $propertyName
     * 
     * @return string
     */
    public function getSetterFunctionName(string $propertyName)
    {
        if (empty($propertyName)) {
            throw new \InvalidArgumentException('Property name cannot be empty string');
        }

        return 'set' . ucfirst($propertyName);
    }

    /**
     * @param string $propertyName
     * 
     * @return string
     */
    public function getGetterFunctionName(string $propertyName)
    {
        if (empty($propertyName)) {
            throw new \InvalidArgumentException('Property name cannot be empty string');
        }
        
        return 'get' . ucfirst($propertyName);
    }

    public function encodeJSON($value) {
        $encoded = json_encode($value);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $encoded;
        } else {
            throw new BadRequestHttpException('Неверный формат значения JSON');
        }
    }

    public function decodeJSON($value) {
        $decoded = json_decode($value, true);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $decoded;
        } else {
            throw new BadRequestHttpException('Неверный формат значения JSON');
        }
    }

    public function moveElement(&$array, $a, $b) {
        $out = array_splice($array, $a, 1);
        array_splice($array, $b, 0, $out);
    }

}