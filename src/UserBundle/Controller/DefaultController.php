<?php

namespace UserBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends FOSRestController
{
    /**
     * @Route("/token/user")
     */
    public function tokenUserAction()
    {
        return $this->get('security.token_storage')->getToken()->getUser();
    }
}
