<?php


namespace SettingsBundle\Entity;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Setting
{
    const SETTING_TYPE_STRING = 1;
    const SETTING_TYPE_ARRAY = 2;
    const SETTING_TYPE_OBJECT = 3;

    const SETTING_NAME_CITY = 'city';
    const SETTING_NAME_ZIP = 'zip';
    const SETTING_NAME_STREET = 'street';
    const SETTING_NAME_PHONES = 'phones';
    const SETTING_NAME_SCHEDULE = 'schedule';
    const SETTING_NAME_EMAIL = 'email';
    const SETTING_NAME_SITE_NAME = 'site_name';
    const SETTING_NAME_PAGE_CONTACTS = 'page_contacts';
    const SETTING_NAME_PAGE_ABOUT_US = 'page_about_us';

    private $id;
    protected $name;
    protected $type;
    protected $value;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Setting
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Setting
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return Setting
     *
     * @throws \Exception
     */
    public function setValue($value)
    {
        $settingType = $this->getType();

        if (!is_null($value)) {
            switch ($settingType) {
                case Setting::SETTING_TYPE_ARRAY:
                    if (!is_array($value)) {
                        throw new \Exception('Wrong value for setting of type ARRAY');
                    }
                    break;
                case Setting::SETTING_TYPE_OBJECT:
                    if (!is_array($value)) {
                        throw new \Exception('Wrong value for setting of type OBJECT');
                    }
                    break;
                case Setting::SETTING_TYPE_STRING:
                    if (!is_scalar($value)) {
                        throw new \Exception('Wrong value for setting of type STRING');
                    }
                    break;
            }
        }

        $this->value = [
            'value' => $value
        ];

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value['value'];
    }

}
