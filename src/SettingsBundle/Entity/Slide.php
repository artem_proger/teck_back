<?php

namespace SettingsBundle\Entity;

class Slide
{
    const POSITION_LEFT = 0;
    const POSITION_RIGHT = 1;

    private $id;
    protected $image;
    protected $title;
    protected $text;
    protected $position = self::POSITION_LEFT;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Slide
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return Slide
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set image.
     *
     * @param json|null $image
     *
     * @return Slide
     */
    public function setImage($image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return json|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Slide
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }
}
