<?php

namespace SettingsBundle\Controller;

use AppBundle\Traits\ControllerHelper;
use SettingsBundle\Entity\Slide;
use SettingsBundle\Form\SlideImageType;
use SettingsBundle\Form\SlideType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class SlideController extends Controller
{
    use ControllerHelper;

    const IMAGE_KEY = 'image';

    public function createAction(Request $request)
    {
        $slide = new Slide();

        $data = $this->getJsonFromRequest($request);

        $form = $this->createForm(SlideType::class, $slide);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->fillSlide($slide, $data);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($slide);
            $entityManager->flush();

            return $this->getApiResponse()->entityResponse($slide, Response::HTTP_CREATED);
        }

        return $this->getApiResponse()->validationFailedResponse($form, 500);
    }


    /**
     * @ParamConverter("slide", class="SettingsBundle:Slide")
     *
     * @param Request $request
     * @param Slide $slide
     *
     * @return array|\FOS\RestBundle\View\View
     */
    public function updateAction(Request $request, Slide $slide)
    {
        $data = $this->getJsonFromRequest($request);

        $form = $this->createForm(SlideType::class, $slide);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->fillSlide($slide, $data);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($slide);
            $entityManager->flush();

            return $this->getApiResponse()->entityResponse($slide);
        }

        return $this->getApiResponse()->validationFailedResponse($form, 501);
    }

    /**
     * @ParamConverter("slide", class="SettingsBundle:Slide")
     * 
     * @param Slide $slide
     *
     * @return \FOS\RestBundle\View\View
     */
    public function detailsAction(Slide $slide)
    {
        return $this->getApiResponse()->entityResponse($slide);
    }

    public function collectionAction()
    {
        $repository = $this->getDoctrine()->getRepository(Slide::class);

        $slides = $repository->findAll();

        return $this->getApiResponse()->collectionResponse($slides);
    }

    /**
     * @ParamConverter("slide", class="SettingsBundle:Slide")
     *
     * @param Request $request
     * @param Slide $slide
     *
     * @return array|\FOS\RestBundle\View\View
     */
    public function imageAction(Request $request, Slide $slide)
    {
        if (!$this->checkFile($request->files->get(self::IMAGE_KEY))) {
            return $this->invalidFileErrorResponse();
        }

        $this->removeOldImageFiles($slide);

        $form = $this->createForm(SlideImageType::class);
        $form->submit($request->files->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFileManager = $this->getImageFileManager();

            $imagePackageData = $imageFileManager
                ->upload(
                    $slide,
                    $request->files->get(self::IMAGE_KEY),
                    $this->getUploadConfigKey()
                );

            $imageFileManager->fillEntityImagePackageData(
                $slide, $imagePackageData, $this->getUploadConfigKey()
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($slide);
            $entityManager->flush();

            return $this->getApiResponse()->entityResponse($slide);
        }

        return $this->getApiResponse()->validationFailedResponse($form, 502);
    }

    /**
     * @ParamConverter("slide", class="SettingsBundle:Slide")
     *
     * @param Slide $slide
     *
     * @return \FOS\RestBundle\View\View
     */
    public function deleteAction(Slide $slide)
    {
        $this->removeOldImageFiles($slide);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($slide);
        $entityManager->flush();

        return $this->getApiResponse()->successResponse(400);
    }

    private function fillSlide(Slide $slide, $data)
    {
        $slide->setTitle($data['title']);
        $slide->setText($data['text']);
        $slide->setPosition($data['position']);
    }

    private function removeOldImageFiles(Slide $slide)
    {
        $this->getImageFileManager()
            ->remove(
                $slide,
                $this->getUploadConfigKey()
            );
    }

    private function getUploadConfigKey()
    {
        return 'slide_image';
    }
}
