<?php


namespace SettingsBundle\Controller;


use AppBundle\Traits\ControllerHelper;
use FOS\RestBundle\Request\ParamFetcher;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use SettingsBundle\Entity\Setting;
use SettingsBundle\Filters\NameInFilter;
use SettingsBundle\Repository\SettingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\Annotations\QueryParam;

class SettingsController extends Controller
{
    use ControllerHelper;

    /**
     * @QueryParam(name="in", requirements="[a-z\,\_]+", description="Fetch only this names", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function indexAction(ParamFetcher $paramFetcher)
    {
        $names = $paramFetcher->get('in');
        $namesArr = $names ? explode(',', $names) : null;

        $settings = $this->getSettingsByNames($namesArr);

        return $this->createResponse($this->transformSettingsToKeyValue($settings));
    }

    public function saveAction(Request $request)
    {
        $data = $this->getJsonFromRequest($request);

        $settingsNames = array_keys($data);
        $settings = $this->getSettingsByNames($settingsNames);

        if (count($settings) === 0) {
            return $this->getApiResponse()->collectionResponse([]);
        }

        $em = $this->getEm();

        foreach ($settings as $setting) {
            /**
             * @var $setting Setting
             */
            $settingName = $setting->getName();
            $value = $data[$settingName];
            $setting->setValue($value);
            $em->persist($setting);
        }

        $em->flush();

        return $this->getApiResponse()->entityResponse(
            $this->transformSettingsToKeyValue($settings)
        );
    }

    private function getSettingsByNames(array $names = null)
    {
        /**
         * @var $repository SettingRepository
         */
        $repository = $this->getDoctrine()->getRepository(Setting::class);
        $repository->addDqlFilter(NameInFilter::class, [NameInFilter::PARAM_NAME => $names]);
        return $repository->findAll();
    }

    private function transformSettingsToKeyValue(array $settings)
    {
        $settingsKeyValue = [];
        foreach ($settings as $setting) {
            $settingsKeyValue[$setting->getName()] = $setting->getValue();
        }

        return $settingsKeyValue;
    }

    private function createResponse($data)
    {
        $context = new SerializationContext();
        $context->setSerializeNull(true);

        /**
         * @var $serializer Serializer
         */
        $serializer = $this->get('jms_serializer');
        $content = $serializer->serialize($data, 'json', $context);

        return new Response($content);
    }

}