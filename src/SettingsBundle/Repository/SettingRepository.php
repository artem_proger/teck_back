<?php


namespace SettingsBundle\Repository;


use Core\FilterRepoBundle\Filter\FilterRepository;

class SettingRepository extends FilterRepository
{

    function getAlias(): string
    {
        return 's';
    }
}