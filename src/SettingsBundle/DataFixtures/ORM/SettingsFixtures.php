<?php


namespace SettingsBundle\DataFixtures\ORM;


use AppBundle\PurgeTablesFixtureInterface;
use CatalogBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use SettingsBundle\Entity\Setting;
use SettingsBundle\Service\SettingsService;

class SettingsFixtures extends Fixture implements PurgeTablesFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /**
         * @var $settingsService SettingsService
         */
        $settingsService = $this->container->get('settings.settings_service');
        $settingsTypesMapping = $settingsService->getSettingsMapping();

        foreach ($settingsTypesMapping as $name => $type) {
            $setting = new Setting();
            $setting->setName($name);
            $setting->setType($type);
            $setting->setValue(null);

            $manager->persist($setting);
            $manager->flush();
        }
        
        
    }

    /**
     * Get entity classes which used in this fixture
     *
     * @return mixed
     */
    public function getEntityClasses(): array
    {
        return [
            Setting::class
        ];
    }
}