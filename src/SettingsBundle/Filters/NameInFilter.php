<?php


namespace SettingsBundle\Filters;


use Core\FilterRepoBundle\Filter\DQL_Filter;

class NameInFilter extends DQL_Filter
{
    const PARAM_NAME = 'name_in';

    public function getCondition(): string
    {
        $nameIn = $this->getParameter(self::PARAM_NAME);

        if (!$this->hasParameter(self::PARAM_NAME)
            || count($nameIn) === 0
            || $nameIn === '') {

            return '';
        }

        $nameIn = array_map(function ($name) {
            return '\'' . $name . '\'';
        }, $nameIn);

        $query = $this->getAlias() . '.name IN (' . implode(', ', $nameIn) .  ')';

        return $query;
    }
}