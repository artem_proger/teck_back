<?php

use SettingsBundle\Entity\Setting;

$settingsMapping = [];

$settingsMapping[Setting::SETTING_NAME_CITY]          = Setting::SETTING_TYPE_STRING;
$settingsMapping[Setting::SETTING_NAME_ZIP]           = Setting::SETTING_TYPE_STRING;
$settingsMapping[Setting::SETTING_NAME_STREET]        = Setting::SETTING_TYPE_STRING;
$settingsMapping[Setting::SETTING_NAME_PHONES]        = Setting::SETTING_TYPE_ARRAY;
$settingsMapping[Setting::SETTING_NAME_SCHEDULE]      = Setting::SETTING_TYPE_ARRAY;
$settingsMapping[Setting::SETTING_NAME_EMAIL]         = Setting::SETTING_TYPE_STRING;
$settingsMapping[Setting::SETTING_NAME_SITE_NAME]     = Setting::SETTING_TYPE_STRING;
$settingsMapping[Setting::SETTING_NAME_PAGE_CONTACTS] = Setting::SETTING_TYPE_STRING;
$settingsMapping[Setting::SETTING_NAME_PAGE_ABOUT_US] = Setting::SETTING_TYPE_STRING;

$container->setParameter('settings.settings_types_mapping', $settingsMapping);