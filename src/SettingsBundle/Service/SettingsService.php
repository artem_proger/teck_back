<?php


namespace SettingsBundle\Service;


use AppBundle\Service\SystemService;
use SettingsBundle\Entity\Setting;

class SettingsService
{
    protected $settingsMapping;

    public function __construct($settingsMapping)
    {
        $this->settingsMapping = $settingsMapping;
    }

    public function getSettingsMapping()
    {
        return $this->settingsMapping;
    }
}